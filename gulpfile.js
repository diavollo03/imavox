
var gulp = require('gulp');
var requirejsOptimize = require('gulp-requirejs-optimize'),

    plugins = require('gulp-load-plugins')({ camelize: true }),
    lr = require('tiny-lr'),
    server = lr(),
    streamqueue  = require('streamqueue');

// // Styles
// gulp.task('styles', function() {
//         return plugins.rubySass('resources/assets/sass/main.sass', {
//                 style: 'expanded',
//         compass: true,
//         sourcemap: true
//         })
//         .on('error', plugins.rubySass.logError)
//         .pipe(gulp.dest('public/assets/css'))
//         .pipe(plugins.minifyCss({ keepSpecialComments: 1 }))
//         // .pipe(plugins.rename({ suffix: '.min' }))
//         // .pipe(gulp.dest('public/assets/css'))
//         .pipe(plugins.notify({ message: 'Styles task complete' }));
// });


gulp.task('scripts', function () {
    return gulp
    	.src('resources/assets/js/app/main.js')
        .pipe(requirejsOptimize(function(file) {
            return {
                // appDir: 'resources/assets/js/app/',
			    baseUrl: 'resources/assets/js/app',
			    // dir: 'public/assets/js',
			    // modules: [
			    //     {
			    //         name: 'app'
			    //     }
			    // ],
			    fileExclusionRegExp: /^(r|build)\.js$/,
			    optimizeCss: 'standard',
			    removeCombined: false,
			    out: 'public/assets/js/build.js',
			    optimize: 'uglify2',
			    uglify2: {
			        //Example of a specialized config. If you are fine
			        //with the default options, no need to specify
			        //any of these properties.
			        output: {
			            beautify: true,
			        },
			        compress: {
			            sequences: false,
			            global_defs: {
			                DEBUG: false
			            }
			        },
			        warnings: true,
			        mangle: false
			    },

			    // bundlesConfigOutFile: './main.js',

			    paths: {
			        app: 'app',
			        // async: '../vendor/async',
			        jquery: '../../../../bower_components/jquery/dist/jquery.min',
			        // jqueryui: '../../vendor/jquery-ui/jquery-ui.min',
			        // jquerymigrate: '../vendor/jquery-migrate/jquery-migrate.min',
			        underscore: '../../../../bower_components/underscore/underscore-min',
			        backbone: '../../../../bower_components/backbone/backbone-min',
			        // bootstrapTable: '../../../../bower_components/bootstrap-table/dist/bootstrap-table.min',
			        bootstrap: '../../../../bower_components/bootstrap/dist/js/bootstrap.min',
			        scrollto: '../../../../bower_components/jquery.scrollTo/jquery.scrollTo.min',
			        // bridget: '../../vendor/jquery-bridget/jquery-bridget',
			        // lload: '../../vendor/jquery_lazyload/jquery.lazyload',
			        // imagesloaded: '../../vendor/imagesloaded/imagesloaded.pkgd.min',
			        'utils': 'utils',
			        // bootstrapselect: '../../vendor/bootstrap-select/dist/js/bootstrap-select.min',
			        // flickity: '../../vendor/flickity/dist/flickity.pkgd.min',
			        // 'flickity-bg-lazyload': '../../vendor/flickity-bg-lazyload/bg-lazyload',
			        // 'flickity/js/index' : '../../vendor/flickity/js/index',
			        // 'fizzy-ui-utils/utils': '../../vendor/fizzy-ui-utils/utils',
			        // 'desandro-matches-selector': '../../vendor/matches-selector',
			        // nicescroll: '../../vendor/jquery.nicescroll/dist/jquery.nicescroll.min',
			        // modernizr: '../../../../bower_components/modernizr-3.5.0.min',
			        detectizr: '../../../../bower_components/detectizr/dist/detectizr.min',
			        // jcookie: '../../vendor/jquery.cookie/jquery.cookie',
			        // wpcr3: 'plugins/wp-customer-reviews'
			        requireLib: '../../../../bower_components/requirejs/require'
			    },
			    include: ['requireLib', 'main.js', 'views/AppView', 'views/page/DefaultView', 'router'],
			    shim: {},
            };
        }))
        .pipe(gulp.dest('./'))
        .pipe(plugins.notify({ message: 'Scripts task complete' }));
});




// Watch
gulp.task('watch', function() {

	// Listen on port 35729
	server.listen(18514, function (err) {
		if (err) {
			return console.log(err)
		};

        // Watch .scss / .sass files
        gulp.watch(['resources/assets/sass/**/*.sass', 'resources/assets/sass/**/*.scss'], ['styles']);

        gulp.watch(['resources/assets/sass/email.sass'], ['emailstyle']);
        
        // gulp.watch(['assets/sass/admin/**/*.sass'], ['adminstyles']);

		// Watch .js files
		gulp.watch('resources/assets/js/app/**/*.js', ['scripts']);

	//         // Watch main.js file for bundle
	//         // gulp.watch(['js/**/*.js', 'js/app/templates/**/*.html', '!js/dist/*', '!js/build/*'], ['bundle']);

	//         // Watch image files
	//         gulp.watch('assets/images/**/*', ['images']);

	});

});


gulp.task('default', ['scripts', 'styles', 'watch']);