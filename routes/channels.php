<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});


Broadcast::channel('presence-mc-chat-conversation.{conversationId}', function ($user) {
    return ['id' => $user->id, 'name' => $user->name];
});


// Broadcast::channel('private-mc-chat-conversation.{conversationId}', function ($user) {
//     return ['id' => $user->id, 'name' => $user->name];
// });


// More information: https://laravel.com/docs/master/broadcasting#authorizing-channels
//
// The user will have already been authenticated by Laravel. In the
// below callback, we can check whether the user is _authorized_ to
// subscribe to the channel.
//
// In routes/channels.php

// Broadcast::channel('private-mc-chat-conversation.{conversationId}', function ($channel_name, $socket_id) {
//   // return $user->id === $userId;
// 	if ( auth()->user() )
// 	{
		
// 	  $pusher = new Pusher('72cb9e67b75a2f120702', 'f9730c1929243c02c917', '966423');
// 	  $auth = $pusher->socket_auth($channel_name, $socket_id);

// 	  $callback = str_replace('\\', '', $_GET['callback']);
// 	  header('Content-Type: application/javascript');
// 	  echo($callback . '(' . $auth . ');');
// 	}
// 	else
// 	{
// 	  header('', true, 403);
// 	  echo "Forbidden";
// 	}
// });

// More information: https://laravel.com/docs/master/broadcasting#authorizing-channels
//
// The user will have already been authenticated by Laravel. In the
// below callback, we can check whether the user is _authorized_ to
// subscribe to the channel.
//
// In routes/channels.php
Broadcast::channel('user.{userId}', function ($user, $userId) {
  return $user->id === $userId;
});

