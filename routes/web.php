<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
use Pusher\Pusher;

Route::get('login/guest', 'LoginController@index')->name('login.guest');
Route::post('login/guest', 'LoginController@loginOrRegister')->name('login.guest');

Route::get('/', 'IndexController@live');

Route::post('/ajax-record-comment', 'CommentsController@ajaxRecordComment');

Route::post('/record-comment', 'CommentsController@recordComment');


Route::group(['middleware' => 'auth'], function() {

	Route::get('/speaker', 'IndexController@speaker');
	Route::get('/ajax-moderation', 'IndexController@ajaxModeration');
	Route::get('/ajax-speaker', 'IndexController@ajaxSpeaker');
	Route::get('/moderation', 'IndexController@moderation');
	Route::post('/ajax-status/{comment}', 'CommentsController@ajaxStatusUpdate');

	Route::post('/ajax-timer', 'SpeakerSessionsController@ajaxUpdateTimer');
	Route::post('/ajax-timer-control', 'SpeakerSessionsController@ajaxUpdateTimerControl');

	Route::get('/backstage', 'SpeakerSessionsController@admin');

	Route::get('/backstage/comment/{comment}/delete', 'CommentsController@destroy');
	Route::get('/backstage/clear-comments', 'CommentsController@destroyAll');
	Route::get('/backstage/export-comments', 'CommentsController@export');
	Route::post('/backstage/saveVideo', 'SpeakerSessionsController@updateVideo');	
	Route::get('/backstage/saveVideo/removeBanner', 'SpeakerSessionsController@removeBanner');	
	// Route::delete('/backstage/delete-image', 'SpeakerSessionsController@deleteImage');	
	


	Route::get('/backstage/users', 'UsersController@index');
	Route::get('/backstage/users/add', 'UsersController@create');
	Route::get('/backstage/users/{user}/edit', 'UsersController@edit');
	Route::get('/backstage/users/{user}/delete', 'UsersController@destroy');
	Route::get('/backstage/users/import', 'UsersController@addImport');
	Route::post('/backstage/users/import', 'UsersController@import');

	Route::post('/backstage/users/add', 'UsersController@store');
	Route::post('/backstage/users/{user}/edit', 'UsersController@update');

	Route::get('/backstage/languages', 'LanguagesController@index');
	Route::get('/backstage/languages/add', 'LanguagesController@create');
	Route::get('/backstage/languages/{language}/edit', 'LanguagesController@edit');
	Route::get('/backstage/languages/{language}/delete', 'LanguagesController@destroy');

	Route::post('/backstage/languages/add', 'LanguagesController@store');
	Route::post('/backstage/languages/{language}/edit', 'LanguagesController@update');

	Route::get('/backstage/logs', 'LogsController@index');
	Route::get('/backstage/logs/export', 'LogsController@export');
	Route::get('/backstage/logs/clearall', 'LogsController@clearAll');

	Route::post('/backstage/files/upload', 'FilesController@save');
	Route::get('/backstage/files/{file}/delete', 'FilesController@destroy');
	Route::get('/backstage/files/{file}/download', 'FilesController@download');


	Route::delete('/chat/conversations/{id}/messages-custom/{message_id}', 'ChatController@deleteMessage');
        // ->name('conversations.messages.destroy');

	Route::post('/user-left/{user}', 'UsersController@exit');


	Route::get('/backstage/survey', 'SurveyEntriesController@index');
	Route::get('/survey', 'SurveyEntriesController@index');

	Route::post('/survey', 'SurveyEntriesController@store');

	Route::get('/backstage/survey/{surveyEntry}/delete', 'SurveyEntriesController@destroy');
	Route::get('/backstage/survey/clearall', 'SurveyEntriesController@destroyAll');
	Route::get('/backstage/survey/export', 'SurveyEntriesController@export');

	Route::get('/survey-check', 'SpeakerSessionsController@checkSurvey');


});
Route::get('/home', 'IndexController@home');

Route::get('/chat', 'ChatController@chat');

Route::get('/test', 'ChatController@test');

Route::post('pusher/auth', function() {  	
	$socket_id = $_POST['socket_id'];
	$channel_name = $_POST['channel_name'];
	$pusher = new Pusher('72cb9e67b75a2f120702', 'f9730c1929243c02c917', '966423');
	$auth = $pusher->socket_auth($channel_name, $socket_id);
	$callback = '';
	// $callback = str_replace('\\', '', $_GET['callback']);
	header('Content-Type: application/javascript');
	echo($callback . '' . $auth . '');
});



Auth::routes([
		'register' => false, // Registration Routes...
		// 'reset' => false, // Password Reset Routes...
    	'verify' => false, // Email Verification Routes...
    ]);

// Route::get('/home', 'HomeController@index')->name('home');
// route to show the login form
Route::get('/loginguest', 'IndexController@showLogin');

// route to process the form
// Route::post('login', 'IndexController@doLogin');

Route::get('/logout', 'IndexController@doLogout');