<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    //
    protected $guarded = [];

    /**
     * Get the language that owns the file.
     */
    public function language()
    {
        return $this->belongsTo('App\Language');
    }
}
