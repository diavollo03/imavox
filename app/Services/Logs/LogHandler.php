<?php 

namespace App\Services\Logs;
use App\Events\Logs\LogMonologEvent;
use App\Log;
use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Formatter\FormatterInterface as FormatterInterface;

class LogHandler extends AbstractProcessingHandler
{
    
    public function __construct($level = Logger::DEBUG)
    {
        parent::__construct($level);
    }
    
    protected function write(array $record) : void
    {
        // Simple store implementation
        $log = new Log();
       
        $log->fill($record['formatted']);
        $log->save();
        
        // Queue implementation
        // event(new LogMonologEvent($record));
    }
    
    /**
     * {@inheritDoc}
     */
    protected function getDefaultFormatter() : FormatterInterface
    {
        return new LogFormatter();
    }

}