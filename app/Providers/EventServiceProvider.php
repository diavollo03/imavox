<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Event::listen('Musonza\Chat\Eventing\MessageWasSent')

        //

        // dd(\Musonza\Chat\Eventing\MessageWasSent::class);

        Event::listen("Illuminate\Auth\Events\Login*", function($eventName, $data){
            \Log::info('AppLogging', ['body' => sprintf("%s logged into %s", $data[0]->user->name, $data[0]->user->language->title), 'type' => 'auth'] );
        });

        Event::listen("Illuminate\Auth\Events\Logout*", function($eventName, $data){
            \Log::info('AppLogging', ['body' => sprintf("%s logged out", $data[0]->user->name, $data[0]->user->email), 'type' => 'auth'] );
        });

        Event::listen("Musonza\Chat\Eventing\MessageWasSent*", function($eventName, $data){
            \Log::info('AppLogging', ['body' => sprintf("%s: %s", $data[0]->message->sender->name, $data[0]->message->body), 'type' => 'chat' ]);
        });
    }
}
