<?php

namespace App\Http\Controllers;

use App\Log;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class LogsController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    	if ( request()->user() )
        { 
            request()->user()->authorizeRoles('admin');
        }

    	$logs = Log::where('description', 'AppLogging')->orderBy('id', 'desc')->paginate(20);

     	return view('logs.index', compact('logs'));
    }


    public function export()
    {
        request()->user()->authorizeRoles('admin');
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=Logs.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $list = Log::where('description', 'AppLogging')->orderBy('id', 'desc')->get();
        $rows = [
            // 'Name'
        ];
        
        if ( $list )
        {
            foreach ( $list as $item )
            {
                $row = [
                    'name' => $item->type,
                    'user' => !empty($item->user) ? $item->user->email : '',
                    'content' => $item->body,
                    'date'  => $item->created_at->format('d-m-Y H:i:s'),
                    'ip'	=> $item->ip,
                    'origin' => $item->origin,
                    'agent' => $item->user_agent
                ];
                $rows[] = $row;
            }
        }

        

        # add headers for each column in the CSV download
        array_unshift($rows, ['Type', 'User', 'Content', 'Date', 'Origin', 'Agent']);

        // return $rows;

        $callback = function() use ($rows) {
            $FH = fopen('php://output', 'w');
            foreach ($rows as $k => $row) { 
            	// $row['body'] = strip_tags($row['content']);
            	// $row['content'] = '1';
            	if ( $k )
            	{ 
            		// print_r($row); die;
            		// unset($row['agent']);
            		// $row['agent'] = '"' . $row['agent'] . "'";
            		// $row['agent'] = htmlentities($row['agent']);
            	}
            	// foreach ( $row as $k1 => $i )
            	// {
            	// 	if ( $k1 != 'user_agent') 

            	// 		$row[$k1] = "$i";
            	// }
            	// if ( $k ) {
            	// 	$row['content'] = '';//html_entities($row['content']);
            	// }
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return (new StreamedResponse($callback, 200, $headers))->send();
    
    }

    public function clearAll()
    {
    	request()->user()->authorizeRoles('admin');
    	$list = Log::where('description', 'AppLogging')->delete();

    	return redirect('backstage/logs');
    }
}
