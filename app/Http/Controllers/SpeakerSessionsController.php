<?php

namespace App\Http\Controllers;

use Validator;

use App\SpeakerSession; 
use App\Comment; 
use App\Language;
use App\SurveyEntry;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use \Carbon\Carbon;

class SpeakerSessionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( \App\Libraries\Helpers $helpers )
    {
        $this->helpers = $helpers;

        $this->middleware('auth');
        $this->middleware('languages');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpeakerSession  $speakerSession
     * @return \Illuminate\Http\Response
     */
    public function show(SpeakerSession $speakerSession)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpeakerSession  $speakerSession
     * @return \Illuminate\Http\Response
     */
    public function edit(SpeakerSession $speakerSession)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpeakerSession  $speakerSession
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpeakerSession $speakerSession)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpeakerSession  $speakerSession
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpeakerSession $speakerSession)
    {
    }

    public function removeBanner(Request $request) 
    {
        $language = $this->helpers->getCurrentLanguage();
        $speakerSession = $this->helpers->getDefaultSpeakerSession();        

        $langSpeakerSession = $language->speakerSessions()->first();


        if ('logo' == $request->banner) {
            if ( $speakerSession->logo_path )
            {
                Storage::delete($speakerSession->logo_path);
                $speakerSession->logo_path = '';
                $speakerSession->save();
            }
        } elseif ( 'footer' == $request->banner ) {
            if ( $speakerSession->banner_path ) {
                Storage::delete($speakerSession->banner_path);
                $speakerSession->banner_path = '';
                $speakerSession->save();
            }
        }   

        return redirect('backstage')->with('message', 'Image removed');

    }

    public function updateVideo(Request $request )
    {

        $language = $this->helpers->getCurrentLanguage();
        $speakerSession = $this->helpers->getDefaultSpeakerSession();        

        $langSpeakerSession = $language->speakerSessions()->first();



        $validator = Validator::make($request->all(), [
            'video_url' => 'nullable|url',
            'enable-chat' => 'integer',
            'enable-messages' => 'boolean',
            'enable-survey' => 'boolean',
            'secondary_color' => 'required',
            'primary_color' => 'required',
            'header_background' => 'required',
            'text_color' => 'required',
            'login_type' => 'integer',
            // 'global-password'  => 'string'

        ]);
        if (! $validator->fails() ) {

            // If the language isn't the default one, it will update the video stream 
            // to the specific speakerSession entry
            // All the rest will remain on the default speakerSession
            if ( $speakerSession->id != $langSpeakerSession->id ) {

                $langSpeakerSession->youtube_url = $request['video_url'];
                $youtube_embed_code = preg_replace(
                                                    "/http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?(.*)?/i",
                                                    "<iframe width=\"100%\" height=\"315\" src=\"//www.youtube.com/embed/$1?rel=0&amp;autoplay=1\" allow=\"autoplay\" frameborder=\"0\" allowfullscreen></iframe>", 
                                                    $langSpeakerSession->youtube_url,
                                                    1
                                                );
                if ( $youtube_embed_code )
                {
                    $langSpeakerSession->youtube_embed_code = $youtube_embed_code;
                } else {
                    $langSpeakerSession->youtube_embed_code = $request['video_embed_code'];
                }
            } else {

                $speakerSession->youtube_url = $request['video_url'];
                $youtube_embed_code = preg_replace(
                                                    "/http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?(.*)?/i",
                                                    "<iframe width=\"100%\" height=\"315\" src=\"//www.youtube.com/embed/$1?rel=0&amp;autoplay=1\" allow=\"autoplay\" frameborder=\"0\" allowfullscreen></iframe>", 
                                                    $speakerSession->youtube_url,
                                                    1
                                                );
                if ( $youtube_embed_code )
                {
                    $speakerSession->youtube_embed_code = $youtube_embed_code;
                } else {
                    $speakerSession->youtube_embed_code = $request['video_embed_code'];
                }
            }

            if ( $request['enable-messages'] )
            {
                $speakerSession->messages = 1;
            } else {
                $speakerSession->messages = 0;
            }

            if ( $request['enable-survey'] )
            {
                $speakerSession->survey = 1;
            } else {
                $speakerSession->survey = 0;
            }

            if ( $request['enable-chat'] )
            {
                $speakerSession->chat = 1;
            } else {
                $speakerSession->chat = 0;
            }

            $speakerSession->header_background = $request['header_background'];
            $speakerSession->text_color = $request['text_color'];
            $speakerSession->primary_color = $request['primary_color'];
            $speakerSession->secondary_color = $request['secondary_color'];

            $speakerSession->login_type = (int) $request['login-type'];
            $speakerSession->password = $request['global-password'];

            // this is set because the field cannot be null
            // it's not actually used at the moment
            $langSpeakerSession->login_type = (int) $request['login-type'];

            // dd(request()->files);
            if ( $request->file('banner-img') )
            {
                $ext = $request->file('banner-img')->getClientOriginalExtension();
                if ( preg_match('/png|jpg|jpeg/i', $ext ) )
                {
                    try {
                        $path = Storage::putFileAs(
                            'public/banners', $request->file('banner-img'), 'footer-banner.' . $ext 
                        );
                        if ( $path )
                        {
                            $speakerSession->banner_path = $path;
                        }
                    } catch (Exception $e ) {

                    }
                }
            }

            if ( $request->file('logo-img') )
            {
                $ext = $request->file('logo-img')->getClientOriginalExtension();
                if ( preg_match('/png|jpg|jpeg/i', $ext ) )
                {
                    try {
                        $path = Storage::putFileAs(
                            'public/images', $request->file('logo-img'), 'logo.' . $ext 
                        );
                        if ( $path )
                        {
                            $speakerSession->logo_path = $path;
                        }
                    } catch (Exception $e ) {

                    }
                }
            }

            // dd($speakerSession);
            $speakerSession->save();
            $langSpeakerSession->save();
        }   



        \Log::info('AppLogging', ['body' => 'Video and settings were updated.']);

        return redirect('backstage')->withErrors($validator);
    }

    public function ajaxUpdateTimer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'timer' => 'required|numeric',
        ]);

        if ( $validator->fails() ) {
            return response()
                ->json(['success' => false, 'errors' => $validator->errors() ]);
        }

        $speaker_session = $this->helpers->getDefaultSpeakerSession();

        
        $speaker_session->timer_interval = $request['timer'];
        // $speaker_session->timer_start_at = date('Y-m-d H:i:s', time() + $request['timer']);
        $speaker_session->timer_active = 0;
        $speaker_session->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $speaker_session->save();
    

        \Log::info('AppLogging', ['body' => 'Timer was updated.']);

        return response()
            ->json(['success' => true ]);
    }

    public function ajaxUpdateTimerControl(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'control' => 'required|numeric',
        ]);

        if ( $validator->fails() ) {
            return response()
                ->json(['success' => false, 'errors' => $validator->errors() ]);
        }
        $control = (int) $request['control'];

        \Log::info('AppLogging', ['body' => 'Timer was ' . ( $control ? 'started' : 'stopped' ).'.']);

        $speaker_session = $this->helpers->getDefaultSpeakerSession();
        $speaker_session->timer_active = $control;

        

        if ( $control == '1' )
        {
            $speaker_session->timer_start_at = Carbon::now()->format('Y-m-d H:i:s');
            // echo Carbon::now()->format('Y-m-d H:i:s'); die;
        } else {
            $speaker_session->timer_stop_at = Carbon::now()->format('Y-m-d H:i:s');
            $speaker_session->timer_interval = $speaker_session->getRemainingInterval();
        }


        $speaker_session->save();

        return response()
            ->json(['success' => true ]);
    }


    public function admin( Request $request ) {
        
        request()->user()->authorizeRoles('admin');
        $comments = Comment::all();

        $language = $this->helpers->getCurrentLanguage();
        // dd($language);
        // dd($language);
        // $language = Language::where('code', '=', $request->session()->get('current_lang'))->first();
        // $language = $language ? $language : Language::where('default', '=', '1')->first();

        // return $comments;
        $speaker_session = $this->helpers->getDefaultSpeakerSession();
        // dd($speaker_session);

        if ( $this->helpers->checkHtml($speaker_session->youtube_embed_code) ) {
            // $youtube_embed_code = $speaker_session->youtube_embed_code;
            // $speaker_session->youtube_embed_code = '<div>Invalid code</div>';
            $output_embed = $speaker_session->youtube_embed_code; 
        } else {
            $output_embed = '<div>Invalid code</div>';
        }

        $lang_speaker_session = SpeakerSession::where('language_id', '=', $language->id)->first();

        if ( $this->helpers->checkHtml($lang_speaker_session->youtube_embed_code) ) {
            // $youtube_embed_code = $speaker_session->youtube_embed_code;
            // $lang_speaker_session->youtube_embed_code = '<div>Invalid code</div>';
            $lang_output_embed = $lang_speaker_session->youtube_embed_code; 
        } else {
            $lang_output_embed = '<div>Invalid code</div>';
        }

        $languages = Language::all();

        $downloads = $language->files()->where('type', '=', 'download')->get();
                            

        return view('pages.admin', compact('speaker_session', 'comments', 'languages', 'language', 'downloads', 'lang_speaker_session', 'output_embed', 'lang_output_embed') );
    }


    public function checkSurvey( Request $request ) {
        $speaker_session = $this->helpers->getDefaultSpeakerSession();
        // dd();
        return response()
                ->json(['enabled' => $speaker_session->survey && null === SurveyEntry::where('user_id', '=', $request->user()->id)->first()]);
    }
}
