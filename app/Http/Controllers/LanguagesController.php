<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Facades\Hash;

use App\Language;
use App\SpeakerSession;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \Carbon\Carbon;


class LanguagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( request()->user() )
        { 
            request()->user()->authorizeRoles('admin');
        }

        $languages = Language::all();


        return view('languages.index', compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        request()->user()->authorizeRoles('admin');

        return view('languages.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles('admin');

        $rules = [
                'title' => 'required|unique:languages',
                'code' => 'required|unique:languages',
            ];
        

        $validator = Validator::make($request->all(), $rules);
        $return = 'backstage/languages';
        $message = false;
        if (! $validator->fails() ) {
            $message = 'Language created.';

            $language = new Language;

            $language->title = $request['title'];
            $language->code  = $request['code'];
            $language->save();
            if ( $language->id ) {
                $speaker_session = SpeakerSession::create([
                    'timer_interval' => 0,
                    'language_id' => $language->id,
                    // 'timer_start_at' => date('Y-m-d H:i:s', time() + $request['timer']),
                    'timer_active' => 0,
                    // 'youtube_url' => '',
                    // 'youtube_embed_code' => '',
                    // 'timer_start_at' => '0000-00-00 00:00:00'
                ]);
            }
            

        } else {
            $return .= '/add';
        }

        return redirect( $return )->withErrors($validator)->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language)
    {
        request()->user()->authorizeRoles('admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $language)
    {
        request()->user()->authorizeRoles('admin');

        // $roles = Role::all();

        return view('languages.edit', compact('language'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language $language)
    {
        $request->user()->authorizeRoles('admin');

        $rules = [
                'title' => 'required|string',
                'code' => 'string|required'
            ];
        if ( $request['title'] != $language->title )
        {
            $rules['title'] = 'required|unique:languages';
        }
        if ( $request['code'] != $language->code )
        {
            $rules['code'] = 'required|unique:languages';
        }

        $validator = Validator::make($request->all(), $rules);

        
        $return = 'backstage/languages';
        $message = false;
        if (! $validator->fails() ) {
            $message = 'Language updated.';

            $language->title = $request['title'];
            $language->code  = $request['code'];
            
            $language->save();
        } else {
            $return .= '/' . $language->id . '/edit';
        }

        return redirect($return)->withErrors($validator)->with('message', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        request()->user()->authorizeRoles('admin');
        if ( $language->id == 1 ){
            return redirect('backstage/languages/')->with('message', 'This language cannot be removed.');            
        }
        if ($language->users->count()) {
            foreach ($language->users as $user ) 
            {
                $user->language_id = 1;
                $user->save();
                \Log::info('AppLogging', ['body' => 'User ' . $user->email . ' was reset to the default language as ' . $language->title .' is going to be deleted']);
            }
        }
        
        if ( $language->files->toArray() ) {
            return redirect('backstage/languages/')->withErrors(['There are files added for this language. Please remove them first.']);
        }

        $language->delete();
        \Log::info('AppLogging', ['body' => 'Language ' . $language->title . ' has been deleted']);

        return redirect('backstage/languages/')->with('message', 'Language removed.');

    }

}
