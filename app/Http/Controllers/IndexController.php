<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \Carbon\Carbon;

use App\Comment;
use App\SpeakerSession;
use App\Language;

use App\Jobs\ProcessUserExit;

use Chat;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

use \Queue;

use Illuminate\Support\Facades\DB;



class IndexController extends Controller
{
    

    public function __construct(\App\Libraries\Helpers $helpers) {
        $this->helpers = $helpers;
    	$this->middleware('auth');
        $this->middleware('languages');
    	
    }
    public function home()
    {
        return redirect('/');
    }

    public function live()
    {

        $speaker_session = $this->helpers->getDefaultSpeakerSession();
        // $language = $this->helpers->getCurrentLanguage();

        // dd(auth()->user());
        
        $data = [
            'speaker_session' => $speaker_session
        ];

        

        if ( auth()->user() )
        {

            $user = auth()->user();

            
            $data['languages'] = Language::all();

            if ( request()->get('lang') )
            {
                $lang_code = request()->get('lang');
                $param_language = Language::where('code', '=', $lang_code)->first();
                // dd($param_language);
                if ( $param_language && $user->language_id != $param_language->id ) {
                    $user->language_id = $param_language->id;
                    $user->save();
                }
            }

            $language = $user->language;
            $lang_speaker_session = $language->speakerSessions()->first();
            $data['lang_speaker_session'] = $lang_speaker_session;

            // dd($user->language->title);
            $data['current_language'] = $user->language;

            // $this->app->bindMethod(ProcessUserExit::class.'@handle', function ($job, $app) {
            //     return $job->handle($app->make(User::class));
            // });
            // dd(now()->addMinutes(1));
            // ProcessUserExit::delete()


            // ProcessUserExit::dispatch($user, time())
            //     ->onQueue('userExit' . $user->id)
            //     ->delay(now()->addSeconds(10));
            //     // ->delay(60);

            // $res = Queue::later(5, ProcessUserExit::class, [], 'userExit' . $user->id);
            //get the job from the que that you just pushed it to
            // $job = Queue::getPheanstalk()->useTube('userExit' . $user->id)->peek($res);
            // dd($job);

            DB::table('jobs')->where('queue', 'userExit' . $user->id)->delete();

            $conversation = Chat::conversations()->conversation->orderby('id', 'desc')->first()->toArray();
            $conversations = Chat::conversations(Chat::conversations()->conversation)
              ->setParticipant(auth()->user())
              ->get()
              ->toArray()['data'];
            
            $conversations = Arr::pluck($conversations, 'conversation_id');
            $conversationId = Chat::conversations(Chat::conversations()->conversation)->get()->toArray();

            $downloads = $language->files ?? $language->files->toArray();

            // dd($downloads);
            $data = array_merge($data, [
                'downloads' => $downloads,
                'conv'  => $conversations,
                'conversations'  => $conversations,
                'conversationId' => $conversation ? $conversation['id'] : null,
                'participant' => [
                    'id' => auth()->user()->id,
                    'type' => get_class(auth()->user())
                ],
            ]);

        }
        
        
        return view('pages.live', $data);
    }


    public function moderation( Request $request) {

        request()->user()->authorizeRoles(['moderator', 'admin']);
        $new_items = Comment::where('status', '=', Comment::STATUS_NEW)->get();
        $live_items = Comment::where('status', '=', Comment::STATUS_LIVE)->get();
        $archived_items = Comment::where('status', '=', Comment::STATUS_ARCHIVED)->get();

        // $speaker_session = SpeakerSession::getLatest();
        $speaker_session = $this->helpers->getDefaultSpeakerSession();
        $clock_timer = Carbon::createFromTimestamp(($speaker_session->timer_active ? $speaker_session->getRemainingInterval() : $speaker_session->timer_interval ) )->format('i:s');

        // $speaker_session = SpeakerSession::getLatest();
        $this->current_language = $this->helpers->getCurrentLanguage();
        $lang_speaker_session = $this->current_language->speakerSessions()->first();

        $data = [
            'speaker_session' => $speaker_session,
            'lang_speaker_session' => $lang_speaker_session,
            'languages' => Language::all()
        ];

        if ( auth()->user() )
        {
            $conversation = Chat::conversations()->conversation->orderby('id', 'desc')->first()->toArray();
            $conversations = Chat::conversations(Chat::conversations()->conversation)
              ->setParticipant(auth()->user())
              ->get()
              ->toArray()['data'];
            
            $conversations = Arr::pluck($conversations, 'conversation_id');
            $conversationId = Chat::conversations(Chat::conversations()->conversation)->get()->toArray();

            $data = array_merge($data, [
                'conv'  => $conversations,
                'conversations'  => $conversations,
                'conversationId' => $conversation ? $conversation['id'] : null,
                'participant' => [
                    'id' => auth()->user()->id,
                    'type' => get_class(auth()->user())
                ],
            ]);

        }

    	return view('pages.moderation', array_merge( $data, compact( 'new_items', 'live_items', 'archived_items', 'speaker_session', 'clock_timer' ) ) );	
    }

    public function ajaxModeration()
    {
        request()->user()->authorizeRoles(['moderator', 'speaker', 'admin']);
        $new_items = Comment::where('status', '=', Comment::STATUS_NEW)->get();
        $live_items = Comment::where('status', '=', Comment::STATUS_LIVE)->get();
        $archived_items = Comment::where('status', '=', Comment::STATUS_ARCHIVED)->get();

        // $speaker_session = SpeakerSession::getLatest();
        $speaker_session = $this->helpers->getDefaultSpeakerSession();
        $current_language = $this->helpers->getCurrentLanguage();
        $lang_speaker_session = $current_language->speakerSessions()->first();
        $clock_timer = Carbon::createFromTimestamp(($speaker_session->timer_active ? $speaker_session->getRemainingInterval() : $speaker_session->timer_interval ) )->format('i:s');
        $ajax = true;
        $languages = Language::all();
        return response()
            ->json([
                'success' => true,
                'html' => view('pages.moderation', compact('new_items', 'live_items', 'archived_items', 'speaker_session', 'clock_timer', 'ajax', 'languages', 'lang_speaker_session'))->render(),
                'speakerSession' => $speaker_session,
                'clockTimer' => $clock_timer                
            ]);
    }


    public function speaker()
    {
        request()->user()->authorizeRoles(['moderator', 'speaker', 'admin']);
        $live_items = Comment::where('status', '=', Comment::STATUS_LIVE)->limit(6)->get();
        // $speaker_session = SpeakerSession::getLatest();
        $speaker_session = $this->helpers->getDefaultSpeakerSession();

        // $clock_timer = Carbon::createFromTimestamp($speaker_session->getRemainingInterval())->format('i:s');
        $clock_timer = Carbon::createFromTimestamp(($speaker_session->timer_active ? $speaker_session->getRemainingInterval() : $speaker_session->timer_interval ) )->format('i:s');

    	return view('pages.speaker', compact('live_items', 'speaker_session', 'clock_timer'));	
    }

    public function ajaxSpeaker()
    {
        request()->user()->authorizeRoles(['moderator', 'speaker', 'admin']);
        $live_items = Comment::where('status', '=', Comment::STATUS_LIVE)->limit(6)->get();
        // $speaker_session = SpeakerSession::getLatest();

        $speaker_session = $this->helpers->getDefaultSpeakerSession();

        $clock_timer = Carbon::createFromTimestamp(($speaker_session->timer_active ? $speaker_session->getRemainingInterval() : $speaker_session->timer_interval ) )->format('i:s');

        return response()
            ->json([
                'success' => true,
                'html' => view('pages.speaker', compact('live_items', 'speaker_session', 'clock_timer'))->render(),
                'speakerSession' => $speaker_session,
                'clockTimer' => $clock_timer                
            ]);
    }

    public function doLogout()
    {
        // echo 1; die;
        \Log::info('AppLogging', ['body' => 'User () has logged out']);

        \Auth::logout(); // log the user out of our application
        return redirect('login'); // redirect the user to the login screen
    }

    public function showLogin()
    {

        $speaker_session = $this->helpers->getDefaultSpeakerSession();
        return view('pages.login', compact('speaker_session'));  
    }

}