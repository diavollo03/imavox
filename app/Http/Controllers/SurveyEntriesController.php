<?php

namespace App\Http\Controllers;

use App\SurveyEntry;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

use Validator;

class SurveyEntriesController extends Controller
{
    //

    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        if ( request()->user() )
        { 
            request()->user()->authorizeRoles('admin');
        }

        $survey_entries = SurveyEntry::where('id', '<>', '0')->orderBy('id', 'desc')->paginate(20);

        return view('survey.index', compact('survey_entries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
                'question-1' => 'required',
                'question-2' => 'required',
                'question-3' => 'required',
                'question-4' => 'required',
                'question-5' => 'required',
                // 'question-6' => 'required',
            ];
        
        $return = '/';
        $validator = Validator::make($request->all(), $rules);
        $message = null;
        if (! $validator->fails() ) {
            

            $survey_entry = new SurveyEntry;

            $survey_entry->question_1 = $request['question-1'];
            $survey_entry->question_2 = $request['question-2'];
            $survey_entry->question_3 = $request['question-3'];
            $survey_entry->question_4 = $request['question-4'];
            $survey_entry->question_5 = $request['question-5'];
            // $survey_entry->question_6 = $request['question-6'];

            $survey_entry->user_id = auth()->user()->id;


            $survey_entry->save();

            \Log::info('AppLogging', ['body' => 'User ' . auth()->user()->email . ' sent a survey form.']);

            if ( $survey_entry->id ) {
                $message = 'Succesfully sent.';
            } else {
                $message = 'Something went wrong';
            }
            

        }

        return redirect( $return )->withErrors($validator)->with('form-message', $message);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  SurveyEntry $surveyEntry
     * @return \Illuminate\Http\Response
     */
    public function destroy(SurveyEntry $surveyEntry) 
    {
        //
        request()->user()->authorizeRoles('admin');
        $surveyEntry->delete();
        $return_url = '/backstage/survey';
        return redirect($return_url)->with('message', 'Entry was succesfully deleted.');
    }

    public function destroyAll(SurveyEntry $surveyEntry) 
    {
        //
        request()->user()->authorizeRoles('admin');
        SurveyEntry::truncate();
        $return_url = '/backstage/survey';
        return redirect($return_url)->with('message', 'All entries were succesfully deleted.');
    }

    public function export()
    {
        request()->user()->authorizeRoles('admin');
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=SurveyEntries.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $list = SurveyEntry::orderBy('id', 'desc')->get();
        $rows = [
            // 'Name'
        ];
        
        if ( $list )
        {
            foreach ( $list as $item )
            {
                $row = [
                    'user' => $item->user->email,
                    'question_1' => $item->question_1,
                    'question_2' => $item->question_2,
                    'question_3' => $item->question_3,
                    'question_4' => $item->question_4,
                    'question_5' => $item->question_5,
                    'date'  => $item->created_at->format('d-m-Y H:i:s'),
                ];
                $rows[] = $row;
            }
        }

        

        # add headers for each column in the CSV download
        array_unshift($rows, ['User', 'Question 1', 'Question 2', 'Question 3', 'Question 4', 'Question 5', 'Date']);

        $callback = function() use ($rows) {
            $FH = fopen('php://output', 'w');
            foreach ($rows as $k => $row) { 
                // $row['body'] = strip_tags($row['content']);
                // $row['content'] = '1';
                if ( $k )
                { 
                    // print_r($row); die;
                    // unset($row['agent']);
                    // $row['agent'] = '"' . $row['agent'] . "'";
                    // $row['agent'] = htmlentities($row['agent']);
                }
                // foreach ( $row as $k1 => $i )
                // {
                //  if ( $k1 != 'user_agent') 

                //      $row[$k1] = "$i";
                // }
                // if ( $k ) {
                //  $row['content'] = '';//html_entities($row['content']);
                // }
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return (new StreamedResponse($callback, 200, $headers))->send();
    
    }
}
