<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Role;
use App\Log;
use App\Imports\UsersImport;
use App\Jobs\ProcessUserExit;

use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \Carbon\Carbon;



class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( request()->user() )
        { 
            request()->user()->authorizeRoles('admin');
        }

        $users = User::where('id', '<>', '0')->paginate(100);;


        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        request()->user()->authorizeRoles('admin');

        $roles = Role::all();

        return view('users.add', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles('admin');

        $rules = [
                'email' => 'required|unique:users|email',
                'name' => 'string|required',
                'role' => 'integer',
                'password' => 'required|confirmed'
            ];
        

        $validator = Validator::make($request->all(), $rules);
        $return = 'backstage/users';
        $message = false;
        if (! $validator->fails() ) {
            $message = 'User created.';

            $user = new User;

            $user->email = $request['email'];
            $user->name  = $request['name'];
            $user->password = Hash::make($request['password']);
            $user->save();
            
            $user
               ->roles()
               ->attach(Role::where('id', $request['role'])->first());

        } else {
            $return .= '/add';
        }

        return redirect( $return )->withErrors($validator)->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        request()->user()->authorizeRoles('admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        request()->user()->authorizeRoles('admin');

        $roles = Role::all();

        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Show the form for import
     *
     * @return \Illuminate\Http\Response
     */
    public function addImport()
    {
        request()->user()->authorizeRoles('admin');

        return view('users.import');
    }

    public function import(Request $request) 
    {


        if ( $request->file('import-file') )
            {
                $ext = $request->file('import-file')->getClientOriginalExtension();
                if ( preg_match('/csv/i', $ext ) )
                {
                    try {
                        $path = Storage::putFileAs(
                            'import', $request->file('import-file'), 'users.' . $ext 
                        );
                        // Excel::import(new UsersImport, $path);
                        (new UsersImport())->import($path, null, \Maatwebsite\Excel\Excel::CSV);
                        return redirect('/backstage/users')->with('message', 'All good!');
                    // } catch(PhpSpreadsheetException $e ) {
                    //     echo $e->getMessage();
                    } catch (\Exception $e ) {
                        return redirect('/backstage/users')->withErrors($e->getMessage());
                        // echo ;
                    }
                }
            }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->user()->authorizeRoles('admin');

        $rules = [
                'user-email' => 'required|email',
                'name' => 'string|required',
                'role' => 'integer',
            ];
        if ( $request['user-email'] != $user->email )
        {
            $rules['user-email'] = 'required|unique:users,email|email';
        }

        if ( $request['change-password'] )
        {
            $rules['password'] = 'required|confirmed';
        }

        $validator = Validator::make($request->all(), $rules);

        
        $return = 'backstage/users';
        $message = false;
        if (! $validator->fails() ) {
            $message = 'User updated.';

            $user->email = $request['user-email'];
            $user->name  = $request['name'];
            $role = Role::find($request['role'])->get();

            if ( $request['change-password'] )
            {
                $user->password = Hash::make($request['password']);
            }

            $user->save();
            $user
                ->roles()->detach();
            $user
               ->roles()
               ->attach(Role::where('id', $request['role'])->first());
        } else {
            $return .= '/' . $user->id . '/edit';
        }

        return redirect($return)->withErrors($validator)->with('message', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        request()->user()->authorizeRoles('admin');

        $user->roles()->detach();
        $user->delete();

        return redirect('backstage/users/')->with('message', 'User removed.');

    }

    public function exit(User $user) {

        // dd($user->email);
        ProcessUserExit::dispatch($user, time())
            ->onQueue('userExit' . $user->id)
            ->delay(now()->addSeconds(10));
        
        // 

        exit();
    }
}
