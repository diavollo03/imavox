<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Comment;

use Symfony\Component\HttpFoundation\StreamedResponse;

class CommentsController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function recordComment(Request $request) {
        $content = $request['content'];   

        $name = $request->user()->name;
        $email = $request->user()->email;

        $validator = Validator::make($request->all(), [
            // 'name' => 'required|max:105',
            'comment' => 'required|max:150',
            'moderator' => 'required|boolean'
        ]);

        if ( $validator->fails() ) {
            return redirect('/')
                ->withErrors($validator);
        }

        $comment = Comment::create([
                            'name' => $name,
                            'content' => $request['comment'],
                            'status' => $request['moderator'] ? Comment::STATUS_LIVE : Comment::STATUS_NEW,
                            'moderator' => $request['moderator']
                        ]);

        \Log::info('AppLogging', ['body' => $name . '(' . $email . ') posted a message: ' . $request['content'], 'type' => 'message'] );

        return redirect('')
            ->with('message', 'Comment was succesfully added.');
            
    }

    public function ajaxRecordComment(Request $request)
    {
     	$content = $request['content'];   

        $name = $request->user()->name;
        $email = $request->user()->email;

     	$validator = Validator::make($request->all(), [
            // 'name' => 'required|max:105',
            'content' => 'required|max:150',
            'moderator' => 'required|boolean'
        ]);

        if ( $validator->fails() ) {
        	return response()
            	->json(['success' => false, 'errors' => $validator->errors() ]);
        }

        $comment = Comment::create([
        					'name' => $name,
        					'content' => $request['content'],
        					'status' => $request['moderator'] ? Comment::STATUS_LIVE : Comment::STATUS_NEW,
        					'moderator' => $request['moderator']
        				]);

        \Log::info('AppLogging', ['body' => $name . '(' . $email . ') posted a message: ' . $request['content'], 'type' => 'message'] );

        return response()
            ->json(['success' => true ]);
            // ->withCallback($request->input('callback'));


    }

    public function ajaxStatusUpdate( Comment $comment, Request $request )
    {
		$tolive = (bool) $request['tolive'];
		if ( $tolive )
		{
            if ( Comment::where('status', '=', Comment::STATUS_LIVE)->count() < 6)
            {
                $comment->status = Comment::STATUS_LIVE;
            } else {
                return response()
                            ->json(['success' => false, 'error' => 'There are already 6 live comments']);                
            }
		} else {
			$comment->status = Comment::STATUS_ARCHIVED;
		}
		$comment->save();

    	return response()
            ->json(['success' => true ]);

    }

    public function export()
    {
        request()->user()->authorizeRoles(['admin', 'moderator']);
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=Comments.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $list = Comment::all();//->toArray();
        $rows = [
            // 'Name'
        ];
        $statuses = [
            Comment::STATUS_LIVE => 'Live',
            Comment::STATUS_NEW => 'New',
            Comment::STATUS_ARCHIVED => 'Archived',
        ];
        if ( $list )
        {
            foreach ( $list as $item )
            {
                $row = [
                    'name' => $item->name,
                    'content' => $item->content,
                    'status' => $statuses[$item->status],
                    'date'  => $item->created_at->format('d-m-Y H:i:s')
                ];
                $rows[] = $row;
            }
        }

        

        # add headers for each column in the CSV download
        array_unshift($rows, ['Name', 'Content', 'Status', 'Date']);

        // return $rows;

        $callback = function() use ($rows) {
            $FH = fopen('php://output', 'w');
            foreach ($rows as $row) { 
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return (new StreamedResponse($callback, 200, $headers))->sendContent();
    
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect('backstage');
    }

    public function destroyAll()
    {
        Comment::truncate();
        return redirect('backstage');
    }
}
