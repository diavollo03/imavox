<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/moderation';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // public function username()
    // {
    //     return 'name';
    // }


    protected function redirectTo()
    {
        if (Auth()->user()->hasRole('speaker'))
        {
            return '/speaker';
        } elseif (Auth()->user()->hasRole('moderator')) {
            return '/backstage';
        }

        return request()->headers->get('referer');
        // dd(request());
        // return request()->requestUri;
        // return '/path';
    }

     public function login(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if(auth()->attempt(['email'=>$request->email,'password'=>$request->password])){

            // if(auth()->user()->confirmed==0){
            //     Auth::logout();
            //     return back()->with('warning', 'Your account has not yet been activated. Please check Your email');
            // }
            if ( auth()->user()->hasRole('moderator') )
            {
                return redirect('/moderation');    
            } elseif ( auth()->user()->hasRole('admin') )
            {
                return redirect('/backstage');    
            }elseif ( auth()->user()->hasRole('speaker') ) {
                return redirect('/speaker');    
            } else {
                return redirect('/');
            }
            
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
     
    }
}
