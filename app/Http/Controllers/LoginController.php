<?php

namespace App\Http\Controllers;

use App\Log;
use App\User;
use App\Role;
use App\Language;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

use Validator;
use Faker;
use Auth;

use Chat;

use App\SpeakerSession; 


class LoginController extends Controller
{
    //

    public function __construct(\App\Libraries\Helpers $helpers) {

        $this->helpers = $helpers;
        // parent::__construct();
    }

    public function index()
    {
    	// $logs = Log::all();
        // $speaker_session = SpeakerSession::getLatest();
        $languages = Language::all();
        $speaker_session = $this->helpers->getDefaultSpeakerSession();
     	return view('pages.login', compact('speaker_session', 'languages'));
    }

    public function loginOrRegister(Request $request) 
    {

        $speaker_session = $this->helpers->getDefaultSpeakerSession();
        
        $rules = [
            'guest-email' => 'required|email',
            'guest-language' => 'required|integer'
        ];

        if ( 1 == $speaker_session->login_type )
        {
            $rules['guest-name'] = 'required';
            $rules['password'] = 'required|regex:^' . $speaker_session->password . '$^';
        }

    	$validator = Validator::make($request->all(), $rules);


        if ($validator->fails() ) {

        	return redirect('login/guest')->withErrors($validator);

        } else {
        	$user = User::where('email', '=', $request['guest-email'])->first();
            $user_lang = Language::find($request['guest-language']);

            if ( 1 == $speaker_session->login_type ) {
        	
                $name = $request['guest-name'];
            	// dd($user->hasRole('moderator'));
            	

            	if (! $user )
            	{
            		// $faker = Faker\Factory::create();
            		$user = User::create([
            			'name' => $request['guest-name'],
            			// 'email' => $faker->unique()->safeEmail,
            			// 'email_verified_at' => now(),
                        'email' => $request['guest-email'],
            			'password' => Hash::make($speaker_session->password),
            			'remember_token' => Str::random(10)
            		]);
            		$user
    		           ->roles()
    		           ->attach(Role::where('name', 'visitor')->first());

                    \Log::info('AppLogging', ['body' => 'A new user was created']);

                    
                    
            	}
            } else {

                if ( preg_match('/(@ap\.jll\.com|@brookfield\.com)$/i', $request['guest-email']) ) {
                    // dd($request['guest-email']);
                    $user = User::where('email', '=', $request['guest-email'])->first();
                    if ( !$user )
                    {
                        $user = User::create([
                            'name' => $request['guest-email'],
                            // 'email' => $faker->unique()->safeEmail,
                            // 'email_verified_at' => now(),
                            'email' => $request['guest-email'],
                            'password' => Hash::make('defaultpassword'),
                            'remember_token' => Str::random(10)
                        ]);
                        $user
                           ->roles()
                           ->attach(Role::where('name', 'visitor')->first());

                        \Log::info('AppLogging', ['body' => 'A new user was created']);

                    }
                }
                
                if ( !$user )
                {
                    return redirect('login/guest')->withErrors(['guest-email' => 'Invalid login']);       
                } 
            }

            if ( $user_lang ) {
                // dd($user->language());
                // dd($user_lang);
                $user->language()->associate($user_lang);
                // \Log::info('AppLogging', ['body' => $user->email . ' logged into ' . $user_lang->title]);
                $user->save();
            }

        	if ( $user->hasRole('moderator') || $user->hasRole('admin') || $user->hasRole('speaker'))
        	{
        		return redirect('login/guest')->withErrors(['guest-email' => 'Please use another email address']);
        	}

        	// dd($user);
        	Auth::login($user);

            \Log::info('AppLogging', ['body' => $user->email . ' got logged in']);


            $user_conversations = Chat::conversations(Chat::conversations()->conversation)
                                ->setParticipant($user)
                                ->get()->toArray()['data'];
                                
            $user_conversations = Arr::pluck($user_conversations, 'conversation_id');

        

            $conversation = Chat::conversations()->conversation->orderby('id', 'desc')->first();
            if ($conversation && ! in_array( $conversation->id, $user_conversations ) )
            {
            
                Chat::conversation($conversation)->addParticipants([$user]);
            }


    		return redirect('/');
        }
    }
}
