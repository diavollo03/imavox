<?php

namespace App\Http\Controllers;

use App\File;
use App\Language;

use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use \Carbon\Carbon;

class FilesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('languages');
    }



    public function save(Request $request) {

    	$request->user()->authorizeRoles('admin');

        $rules = [
                'download-file' => 'required|file',
                'caption' => 'required|string',
            ];
        

        $validator = Validator::make($request->all(), $rules);
        $return = 'backstage/#downloads';
        $message = false;
        if (! $validator->fails() ) {


        	if ( $request->file('download-file') )
	        {
	            $ext = $request->file('download-file')->getClientOriginalExtension();
	            $filename = $request->file('download-file')->getClientOriginalName();
	            # if ( preg_match('/png|jpg|jpeg/i', $ext ) )
	            // {
	                try {
	                    $path = Storage::putFileAs(
	                        'public/downloads', $request->file('download-file'), $filename 
	                    );
	                    if ( $path )
	                    {
	                    	// dd($request->session()->get('current_lang'));
	                    	$language = Language::where('code', '=', $request->session()->get('current_lang'))->first();
	                    	$language = $language ? $language : Language::where('default', '=', '1')->first();

	                    	$message = 'File uploaded succesfully.';
	                        $file = new File;
							$file->language()->associate($language);
				            $file->path = $path;
				            $file->filename = $filename;
				            $file->caption  = $request['caption'];
				            $file->save();
				            
	                    }
	                } catch (Exception $e ) {
	                	return redirect( $return )->withErrors($e->getMessage)->with('message', $message);
	                	// $message = 'An error has occured: ' . $e->getMessage();
	                }	
	            // }
	        }
        
        } 

        return redirect( $return )->withErrors($validator)->with('message', $message);
    	
    	

    }

    public function download(File $file, Request $request) {
    	return Storage::download( $file->path, $file->filename );
    }

    public function destroy(File $file, Request $request) {
    	$request->user()->authorizeRoles('admin');

    	$message = 'The file (' . $file->filename . ') was deteleted.';
    	$return = 'backstage/#downloads';

    	Storage::delete($file->path);
    	$file->delete();

    	return redirect( $return )->with('message', $message);
    }
}
