<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Chat;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

use Musonza\Chat\Http\Requests\DeleteMessage;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function chat()
    {
        
        $conversations = Chat::conversations(Chat::conversations()->conversation)
          ->setParticipant(auth()->user())
          ->get()
          ->toArray()['data'];
        // dd($conversations);
        $conversations = Arr::pluck($conversations, 'conversation_id');

        


        $data = [
            'conv'  => $conversations,
            'conversations' => array_map('intval', $conversations),
            'participant' => [
                'id' => auth()->user()->id,
                'type' => get_class(auth()->user())
            ]
        ];

        // echo '<pre>'; print_r($data); echo '</pre>';

        return view('pages.chat', $data);
    }

    public function test()
    {
        $data = [];
        return view('pages.test', $data);
    }


    public function deleteMessage(DeleteMessage $request, $conversationId, $messageId)
    {
        $message = Chat::messages()->getById($messageId);
        $msg_service = Chat::message($message);

        $conversation = Chat::conversations()->getById($conversationId);

        $participants = $conversation->getParticipants();
        
        \Log::info('AppLogging', ['body' => $request->getParticipant()->name . ' deleted ' . $message->sender->name . '\'s  message (' . $message->body . ')']);

        foreach( $participants as $participant )
        {
            $msg_service->setParticipant( $participant )->delete();            
        }
        // $msg->delete();

        return response('');
    }

}