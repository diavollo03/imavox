<?php

namespace App\Http\Middleware;

use Closure;
use App\Language;

class SetLanguageSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $current_lang = $request->session()->get('current_lang', 'en');
        if ( $request['lang'] && preg_match('/[a-z\_\-]/i', $request['lang']) ) {
            $lang_code = $request['lang'];
            $language = Language::where('code', '=', $lang_code)->first();
            if ( $language ) {
                $current_lang = $lang_code;
            }
            
        }

        $request->session()->put('current_lang', $current_lang);

        return $next($request);
    }
}
