<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {

            // dd($request);

            // return route('login');
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            elseif ('/' === $request->path())
            {
                // dd(auth()->user());
                return route('login.guest');
            }  else {
                return route('login');
            }
        }
    }


    // public function handle($request, Closure $next, ...$guards)
    // {
    //     // if ( '' = $request->requestUri )

    //     if ($this->auth->guest())
    //     {
    //         if ($request->ajax())
    //         {
    //             return response('Unauthorized.', 401);
    //         }
    //         elseif ('/' === $request->path())
    //         {
    //             return route('loginguest');
    //         }  else {
    //             return route('login');
    //         }
    //     }

    //     return $next($request);
    // }
}
