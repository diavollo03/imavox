<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use \Carbon\Carbon;
use \Carbon\CarbonInterval;

class SpeakerSession extends Model
{
    //

    const TYPE_EMAIL_LOGIN = 1;
    const TYPE_FREE_LOGIN = 2;

    protected $guarded = ['timer_start_at'];
    // protected $table = 'speaker_sessions';

    public static function getLatest()
    {
        $speaker_session = self::latest()->first();

        if ( !$speaker_session )
        {
            $speaker_session = SpeakerSession::create([
                    'timer_interval' => 0,
                    'language_id' => 1,
                    // 'timer_start_at' => date('Y-m-d H:i:s', time() + $request['timer']),
                    'timer_active' => 0,
                    // 'youtube_url' => '',
                    // 'youtube_embed_code' => '',
                    // 'timer_start_at' => '0000-00-00 00:00:00'
                ]);
        }
        return $speaker_session;
    }
    

    public function getRemainingInterval()
    {
        if ( $this->timer_start_at )
        {
            // echo Carbon::createFromDateString($this->timer_start_at)->addSeconds($this->timer_interval)->toTimeString(); die;
            // CarbonInterval::createFromDateString($this->timer_start_at)
        	// CarbonInterval$interval = CarbonInterval::createFromDateString($this->timer_start_at)->addSeconds($this->timer_interval)->toTimeString() - Carbon::now()->toTimeString();
            $expire_time = Carbon::createFromFormat('Y-m-d H:i:s', $this->timer_start_at)->addSeconds($this->timer_interval);
            $now = Carbon::now();
            $interval = $now->diffInSeconds($expire_time);

            if ( $interval < 0 )
            {
                $interval = -1;
            }
        } 
        else 
        {
            $interval = $this->timer_interval;
        }
        return $interval;
    }


}
