<?php

namespace App\Imports;

use App\User;
use App\Role;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsFailures;

use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Row;

use Maatwebsite\Excel\Events\BeforeImport;

use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

use Maatwebsite\Excel\Concerns\Importable;



// class UsersImport implements ToModel, WithHeadingRow
class UsersImport implements OnEachRow, WithValidation, SkipsOnFailure, WithHeadingRow, WithCustomCsvSettings
{

    use SkipsFailures, Importable;

    // /**
    // * @param array $row
    // *
    // * @return \Illuminate\Database\Eloquent\Model|null
    // */
    // public function model(array $row)
    // {
    //     // dd($row);

    //     $user = new User([
    //         //

    //         'name'     => $row['name'],
    //         'email'    => $row['email'], 
    //         'password' => Hash::make('at_password'),
    //     ]);

    //     $user
    //            ->roles()
    //            ->attach(Role::where('name', 'visitor')->first());

    //     return $user;
    // }

    // public static function beforeImport(BeforeImport $event)
    // {
    //     $worksheet = $event->reader->getActiveSheet();
    //     $highestRow = $worksheet->getHighestRow(); // e.g. 10

    //     if ($highestRow < 2) {
    //         $error = \Illuminate\Validation\ValidationException::withMessages([]);
    //         $failure = new Failure(1, 'rows', [0 => 'Now enough rows!']);
    //         $failures = [0 => $failure];
    //         throw new ValidationException($error, $failures);
    //     }
    // }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function onRow(Row $row)
    {
        // dd($row);
        // try {
        $index = $row->getIndex();
        $row = $row->toArray();

        // $user_check = User::where('email', '=', $row['email'])

        
        if ( !array_key_exists('name', $row) || !array_key_exists('email', $row) || !array_key_exists('at_password', $row) ) 
        {
            throw new \Exception('Invalid import file.');
            // return false;
        } 

        $user = new User([
            //
            'name'     => $row['name'],
            'email'    => $row['email'], 
            'password' => Hash::make($row['at_password']),
        ]);
        $user->save();
        
        $user
               ->roles()
               ->attach(Role::where('name', 'visitor')->first());
           // } catch (PhpSpreadsheetException $e ) {
            // dd(1);
           // }
        // return $user;
    }

    public function getCsvSettings(): array {
        return [
            // 'input_encoding' => 'ISO-8859-1'
            'delimiter' => ';'
        ];
    }

    public function rules(): array
    {
        return [
            'email' => Rule::unique('users') //Rule::in(['patrick@maatwebsite.nl']),

             // // Above is alias for as it always validates in batches
             // '*.1' => Rule::in(['patrick@maatwebsite.nl']),
             
             // // Can also use callback validation rules
             // '0' => function($attribute, $value, $onFailure) {
             //      if ($value !== 'Patrick Brouwers') {
             //           $onFailure('Name is not Patrick Brouwers');
             //      }
             //  }
        ];
    }

    // public function onError(\Throwable $e) {
    //     return true;
    // }
}
