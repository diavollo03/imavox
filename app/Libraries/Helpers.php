<?php 

namespace App\Libraries;

use App\Language;
use App\SpeakerSession;

class Helpers {

	public static function getCurrentLanguage() {
		$language = Language::where('code', '=', request()->session()->get('current_lang'))->first();
        $language = $language ? $language : Language::where('default', '=', '1')->first();
        return $language;
	}	

	public static function getDefaultLanguage() {
		
        return Language::where('default', '=', '1')->first();
        
	}	

	public static function getDefaultSpeakerSession() {
		
		$language = self::getDefaultLanguage();
        if ( $language )
        {
        	return SpeakerSession::where('language_id', '=', $language->id)->first();	
        }
        return false;

	}

	public static function checkHtml($string) {
	  	$start =strpos($string, '<');
	  	$end  =strrpos($string, '>',$start);

	  	$len=strlen($string);

	  	if ($end !== false) {
	    	$string = substr($string, $start);
	  	} else {
		    $string = substr($string, $start, $len-$start);
	  	}
	  	libxml_use_internal_errors(true);
	  	libxml_clear_errors();
	  	$xml = simplexml_load_string($string);
	  	$errors = libxml_get_errors();
	  	if ( $errors ) {
	  		foreach( $errors as $error ) {
	  			if ( preg_match('/Extra content at the end of the document/i', $error->message) ) {
	  				return false;
	  			} 
	  		}
	  	}
	  	return true;
	  	// return count(libxml_get_errors()) == 0;
	}

}