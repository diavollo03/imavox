<?php

namespace App\Jobs;

use App\Log;
use App\User;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessUserExit implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user, $time;

    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @return void
     */
    public function __construct(User $user, $time = false)
    {
        $this->user = $user;
        $this->time = $time;
    }

    /**
     * Execute the job.
     *
     * @param  Log  $log
     * @return void
     */
    public function handle(User $user)
    {
        // Process uploaded podcast...
        \Log::info('AppLogging', ['body' => $this->user->email . ' has closed the window at ' . date('Y-m-d H:i:s', $this->time)]);
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        // Send user notification of failure, etc...
    }
}