<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    //
    protected $guarded = [];

    /**
     * Get the files for the language.
     */
    public function files()
    {
        return $this->hasMany('App\File');
    }

    /**
     * Get the SpeakerSessions for the language.
     */
    public function speakerSessions()
    {
        return $this->hasMany('App\SpeakerSession');
    }


    public function users()
    {
        return $this->hasMany('App\User');
    }
}
