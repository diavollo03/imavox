const mix = require('laravel-mix');

// mix.config.fileLoaderDirs.fonts = 'assets/fonts';

// mix.webpackConfig({
//     module: {
//         rules: [
//             {
//                 test: /\.(ttf|eot|woff|woff2)$/,
//                 use : {
//                     loader : "file-loader",
//                     options: {
//                         name: "public/assets/fonts/[name].[ext]",
//                     },
//                 }
//             }
//         ]
//     }
// });

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.js('resources/assets/js/app/main.js', 'public/js')
   	.sass('resources/assets/sass/main.sass', 'public/assets/css');
