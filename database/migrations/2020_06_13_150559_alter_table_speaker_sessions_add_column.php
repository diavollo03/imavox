<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSpeakerSessionsAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('speaker_sessions', function (Blueprint $table) {
            //
            $table->smallInteger('login_type')->default('0');
            $table->string('password', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('speaker_sessions', function (Blueprint $table) {
            //
            $table->dropColumn('login_type');
            $table->dropColumn('password');
        });
    }
}
