<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logs', function (Blueprint $t) {
            //
            // $t->dropColumn('type');

            $t->text('description')->nullable();
            $t->string('origin', 200)->nullable();
            // $t->enum('type', ['log', 'store', 'change', 'delete'])->change();
            $t->enum('result', ['success', 'neutral', 'failure']);
            $t->enum('level', ['emergency', 'alert', 'critical', 'error', 'warning', 'notice', 'info', 'debug']);
            $t->string('token', 100)->nullable();
            $t->ipAddress('ip');
            $t->string('user_agent', 200)->nullable();
            $t->string('session', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function (Blueprint $table) {
            //
        });
    }
}
