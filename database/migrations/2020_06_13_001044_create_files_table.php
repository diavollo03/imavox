<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('language_id')->unsigned();
            $table->string('path');
            $table->string('filename');
            $table->string('type', 40)->default('download');
            $table->string('caption', 255)->nullable();
            $table->text('description')->nullable();
            $table->text('meta')->nullable();

            $table->foreign('language_id')->references('id')
                    ->on('languages')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
