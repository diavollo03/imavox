<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSpeakerSessionAddColumnLangId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('speaker_sessions', function (Blueprint $table) {
            //

            $table->integer('language_id')->index()->unsigned()->default('1')->after('id');
            $table->foreign('language_id')
                    ->references('id')
                    ->on('languages')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

        });

        
        // $seeder = new LanguagesSeeder();
        // $seeder->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('speaker_sessions', function (Blueprint $table) {
            //
            $table->dropColumn('language_id');

        });
    }
}
