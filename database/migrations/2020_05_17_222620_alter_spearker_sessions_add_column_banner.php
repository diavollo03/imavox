<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSpearkerSessionsAddColumnBanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('speaker_sessions', function (Blueprint $table) {
            //

            $table->string('banner_path', 255)->nullable();
            $table->string('logo_path', 255)->nullable();
            $table->string('header_background', 20)->nullable();
            $table->string('text_color', 20)->nullable();
            $table->string('primary_color', 20)->nullable();
            $table->string('secondary_color', 20)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('speaker_sessions', function (Blueprint $table) {
            
           $table->dropColumn('banner_path');
           $table->dropColumn('logo_path');
           $table->dropColumn('header_background');
           $table->dropColumn('text_color');
           $table->dropColumn('primary_color');
           $table->dropColumn('secondary_color');
           
        });
    }
}
