<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpeakerSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speaker_sessions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('youtube_url', 255)->default('');
            $table->string('youtube_embed_code', 255)->default('');
            $table->double('timer_interval')->default(0);
            $table->datetime('timer_start_at')->nullable()->default(null);
            $table->datetime('timer_stop_at')->nullable()->default(null);
            $table->smallinteger('timer_active')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speaker_sessions');
    }
}
