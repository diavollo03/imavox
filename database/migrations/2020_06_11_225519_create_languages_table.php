<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {

            $table->increments('id')->index();
            $table->string('title', 90);
            $table->string('code', 10)->unique();
            $table->smallInteger('default')->default(0);
            $table->timestamps();

        });
        Artisan::call('db:seed', array('--class' => 'LanguagesSeeder'));

        // Artisan::call('db:seed', array('--class' => 'LanguagesSeeder'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
