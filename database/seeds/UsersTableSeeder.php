<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

    	$now = Carbon\Carbon::now();
		

        DB::table('users')->insert([
            'name' => 'Jonas Vernier',
            'email' => 'service@imavox.global',
            'password' => bcrypt('randompassword'),
            'created_at' => $now->toDateTimeString()
        ]);
        DB::table('users')->insert([
            'name' => 'Razvan Merla',
            'email' => 'me@razvanmerla.com',
            'password' => bcrypt('randompassword'),
            'created_at' => $now->toDateTimeString()
        ]);
    }
}
