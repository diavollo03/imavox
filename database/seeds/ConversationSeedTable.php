<?php

use Illuminate\Database\Seeder;
use Musonza\Chat\ConfigurationManager;

class ConversationSeedTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $now = Carbon\Carbon::now();
		

        DB::table(ConfigurationManager::CONVERSATIONS_TABLE)->insert([
            'data' => '[]',
            'created_at' => $now->toDateTimeString()
        ]);
    }
}
