<?php

use Illuminate\Database\Seeder;

class SpeakerSessionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $now = Carbon\Carbon::now();
		

        DB::table('speaker_sessions')->insert([
            'language_id' => '1',
            'created_at' => $now->toDateTimeString()
        ]);
    }
}
