<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;

class UserRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $role_moderator = Role::where('name', 'admin')->first();
		
		$user = User::where('email', 'service@imavox.global')->first();
		$user->roles()->attach($role_moderator);

		$user = User::where('email', 'me@razvanmerla.com')->first();
		$user->roles()->attach($role_moderator);

    }
}
