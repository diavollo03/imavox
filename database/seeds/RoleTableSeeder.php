<?php


use Illuminate\Database\Seeder;
use App\Role;
class RoleTableSeeder extends Seeder
{
  public function run()
  {
    $role_manager = new Role();
    $role_manager->name = 'admin';
    $role_manager->description = 'Admin';
    $role_manager->save();
    $role_employee = new Role();
    $role_employee->name = 'moderator';
    $role_employee->description = 'Moderator';
    $role_employee->save();
    $role_manager = new Role();
    $role_manager->name = 'visitor';
    $role_manager->description = 'Visitor';
    $role_manager->save();
    $role_manager = new Role();
    $role_manager->name = 'speaker';
    $role_manager->description = 'Speaker';
    $role_manager->save();
    
  }
}