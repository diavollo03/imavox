<?php

use Illuminate\Database\Seeder;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

    	$now = Carbon\Carbon::now();

        DB::table('languages')->insert([
            'title' => 'English',
            'code' => 'en',
            'default' => '1',
            'created_at' => $now->toDateTimeString()
        ]);
        

    }
}
