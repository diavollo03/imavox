// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// require('axios');
// import Echo from 'laravel-echo';

// window.Auth = {
    
// }

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// console.log(eval(files))
// files.keys().map(key => {
// 	Vue.component(key.split('/').pop().split('.')[0], files(key))
// })

// Vue.component('chat-conversations', require('./components/ChatConversations.vue').default);
// Vue.component('chat-form', require('./components/ChatForm.vue').default);
// Vue.component('chat-messages', require('./components/ChatMessages.vue').default);

// Vue.component('conversation-participants', require('./components/ConversationParticipants.vue').default);



// window.Echo = new Echo({
//   broadcaster: 'pusher',
//   key: 'APP_KEY',
//   forceTLS: true,
//   authEndpoint: '/broadcasting/auth'
// });

if ( document.getElementById('chatapp') )
{
	// const files = require.context('./', true, /\.vue$/i)
	// files.keys().map(key => {
	// 	console.log(key)
	// 	console.log(key.split('/').pop().split('.')[0]);
	// 	Vue.component(key.split('/').pop().split('.')[0], files(key))

	// })
	Vue.component('chat-conversations', require('./components/ChatConversations.vue').default);
	Vue.component('chat-form', require('./components/ChatForm.vue').default);
	Vue.component('chat-messages', require('./components/ChatMessages.vue').default);



	const app = new Vue({
	  el: '#chatapp'
	});
}