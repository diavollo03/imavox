
var AppRouter = Backbone.Router.extend({

    routes: 
    {
        // Define some URL routes
        '(/)'                                       : 'home',
 
        'customers/add'                             : 'customersAdd',
        'customers/edit/(:id)'                      : 'customersEdit',

        // Pages
        ':page(/:slug1)(/:slug2)(/:slug3)(/)'       : 'page',

        // Search
        // 'search/(:q)(/)'                            : 'search',
        
        // Default
        '*actions'                                  : 'defaultAction'
    },

    initialize: function( options ) {

        this.viewManager = options.viewManager;
        
        //this.routesHit = 0;
        return Backbone.history.on('route', function(e) {
            
            Site.routesHit++;
            
            // var matches = Backbone.history.fragment.match('search/(.+)/?');
            
            // if ( matches != null && matches.length > 1) {
            //     ga('send', 'pageview', '?s=' + matches[1] );
            // } else {
            //     ga('send', 'pageview', '/' + Backbone.history.fragment );
            // }
            //if (_gaq) {
                //  return _gaq.push(["_trackPageview", "/" + Backbone.history.fragment]);
            //}
        }, this);
    },

    stopZombies: function(objView){
        if(typeof objView === "object")
        {
            // alert(1);
            objView.undelegateEvents();
            objView.unbind();
            // $(objView.el).empty();
        }
    }
});

function ViewManager(){

    this.run = function (view) {
        if (this.currentView){
            this.currentView.close();
        }

        this.currentView = view;
        this.currentView.render();

        //$("#mainContent").html(this.currentView.el);
    }
};

export var Router = {
    
    
//     Backbone.View.prototype.close = function(){
//         //this.remove();
//         this.unbind();
//         if ( this.onClose ){
//             this.onClose();
//         }
//     };

//     var AppRouter = Backbone.Router.extend({
//         // _extractParameters: function(route, fragment) {
//         //     var result = route.exec(fragment).slice(1);
//         //     result.unshift(deparam(result[result.length-1]));
//         //     return result.slice(0,-1);
//         // },
//         routes: {
//             // Define some URL routes
//             // '(/)'                                       : 'intro',

//             '(/)'                                     : 'home',
        
            
//             // Pages
//             ':page(/:slug1)(/:slug2)(/:slug3)(/:slug4)(/:slug5)(/:slug6)(/)'       : 'page',

//             // Search
//             // 'search/(:q)(/)'                            : 'search',
            
//             // Default
//             '*actions'                                  : 'defaultAction'
//         },

//         initialize: function( options ) {

//             this.viewManager = options.viewManager;

//             //this.routesHit = 0;
//             return Backbone.history.on('route', function(e) {
                
//                 Site.routesHit++;
                
//                 // var matches = Backbone.history.fragment.match('search/(.+)/?');
                
//                 // if ( matches != null && matches.length > 1) {
//                 //     ga('send', 'pageview', '?s=' + matches[1] );
//                 // } else {
//                 //     ga('send', 'pageview', '/' + Backbone.history.fragment );
//                 // }
//                 //if (_gaq) {
//                     //  return _gaq.push(["_trackPageview", "/" + Backbone.history.fragment]);
//                 //}
//             }, this);
//         },

//         stopZombies: function(objView){
//             if(typeof objView === "object")
//             {
//                 // alert(1);
//                 objView.undelegateEvents();
//                 objView.unbind();
//                 // $(objView.el).empty();
//             }
//         }
//     });
    
    
  
    // var initialize = function(){
    initialize: function(){

        var app_router = new AppRouter({
            viewManager: new ViewManager()
        });
    
       
        !Site.debug || console.log('appView render');
        var app = new AppView();
        window.app_view = app;
        //app.initialize();
        app.render();
        
        
        // Page
        app_router.on('route:home', function(page, subpage){
            // alert(1);
            !Site.debug || console.log('route:: general page', page, subpage);
            var _this = this;
            this.stopZombies(this.lastView);
            // requirejs(['views/page/DefaultView'], function(PageView){
            var DefaultView = require('./views/page/DefaultView');
            var pageView = new DefaultView();
            pageView.render({
                    'page': 'home'
                    // 'subpage': subpage 
                });
            _this.lastView = pageView;    
            // });
        });

        
        // Page
        app_router.on('route:page', function(page, subpage){
            // alert(1);
            !Site.debug || console.log('route:: general page', page, subpage);
            var _this = this;
            this.stopZombies(this.lastView);
            // requirejs(['views/page/DefaultView'], function(PageView){
            var DefaultView = require('./views/page/DefaultView');
            var pageView = new DefaultView();
            pageView.render({
                    'page': page, 
                    'subpage': subpage 
                });
            _this.lastView = pageView;    
            // });
        });

    


        Backbone.history.start({
                pushState: true, 
                hashChange: false,
                root: '/',
                silent: !1
        });

    },

    
    // return { 
    //     initialize: initialize
    // }
}

// simplified $.deparam analog
var deparam = function(paramString){
    var result = {};
    if( ! paramString){
        return result;
    }
    $.each(paramString.split('&'), function(index, value){
        if(value){
            var param = value.split('=');
            result[param[0]] = param[1];
        }
    });
    return result;
};

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function URLParser(u){
    var path="",query="",hash="",params;
    if(u.indexOf("#") > 0){
        hash = u.substr(u.indexOf("#") + 1);
        u = u.substr(0 , u.indexOf("#"));
    }
    if(u.indexOf("?") > 0){
        path = u.substr(0 , u.indexOf("?"));
        query = u.substr(u.indexOf("?") + 1);
        params= query.split('&');
    }else
        path = u;
    return {
        getHost: function(){
            var hostexp = /\/\/([\w.-]*)/;
            var match = hostexp.exec(path);
            if (match != null && match.length > 1)
                return match[1];
            return "";
        },
        getPath: function(){
            var pathexp = /\/\/[\w.-]*(?:\/([^?]*))/;
            var match = pathexp.exec(path);
            if (match != null && match.length > 1)
                return match[1];
            return "";
        },
        getHash: function(){
            return hash;
        },
        getParams: function(){
            return params
        },
        getQuery: function(){
            return query;
        },
        setHash: function(value){
            if(query.length > 0)
                query = "?" + query;
            if(value.length > 0)
                query = query + "#" + value;
            return path + query;
        },
        setParam: function(name, value){
            if(!params){
                params= new Array();
            }
            params.push(name + '=' + value);
            for (var i = 0; i < params.length; i++) {
                if(query.length > 0)
                    query += "&";
                query += params[i];
            }
            if(query.length > 0)
                query = "?" + query;
            if(hash.length > 0)
                query = query + "#" + hash;
            return path + query;
        },
        getParam: function(name){
            if(params){
                for (var i = 0; i < params.length; i++) {
                    var pair = params[i].split('=');
                    if (decodeURIComponent(pair[0]) == name)
                        return decodeURIComponent(pair[1]);
                }
            }
            console.log('Query variable %s not found', name);
        },
        hasParam: function(name){
            if(params){
                for (var i = 0; i < params.length; i++) {
                    var pair = params[i].split('=');
                    if (decodeURIComponent(pair[0]) == name)
                        return true;
                }
            }
            console.log('Query variable %s not found', name);
        },
        removeParam: function(name){
            query = "";
            if(params){
                var newparams = new Array();
                for (var i = 0;i < params.length;i++) {
                    var pair = params[i].split('=');
                    if (decodeURIComponent(pair[0]) != name)
                          newparams .push(params[i]);
                }
                params = newparams;
                for (var i = 0; i < params.length; i++) {
                    if(query.length > 0)
                        query += "&";
                    query += params[i];
                }
            }
            if(query.length > 0)
                query = "?" + query;
            if(hash.length > 0)
                query = query + "#" + hash;
            return path + query;
        },
    }
}

// Default export
export default {
  // Some data...

};