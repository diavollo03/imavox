const _ = require('underscore');
require('bootstrap');
require('bootstrap-colorpicker');
require('../../utils');
// define([
// 	'jquery',
// 	'underscore',
//     'backbone',
//     'views/appView',
    
//     'scrollto',
//     '../../utils',
//     // 'imagesloaded',
//     'bootstrap', 
//     // 'bootstrapTable'
    
// 	], function( $, _, Backbone, AppView, ScrollMagic){

	const DefaultView = Backbone.View.extend({
		el: '#content',

		initialize: function() {
			var _this = this;
		

			$(window).on('resize', _.bind(this._onWindowResize, this));
		},

		events: {
			'change .language-wrap select#language' : 'languageSelectHandler',
			'change select#login-type' 				: 'loginTypeSelectHandler',
			'click .close-popup-form'				: 'closeFormHandler',
		},	


		closeFormHandler: function(e){
			e.preventDefault()
			$('.popup-survey').removeClass('open')
			return false;
		},

		loginTypeSelectHandler: function(e) {
			e.preventDefault()
			var $s = $(e.currentTarget),
				val = $('option:selected', e.currentTarget).val()
			if ( val == 1 ) {
				$('.global-pass-container').show()
			} else {
				$('.global-pass-container').hide()
			}
			return false;
		},

		languageSelectHandler(e) {
			var $s = $(e.currentTarget),
				val = $('option:selected', e.currentTarget).val(),
				url = Backbone.history.fragment,
				hash = window.location.hash
			if ( val ) {
				let href = window.location.href
				if (hash) {
					href = href.split('#')[0]
				}
				document.location.href = replaceUrlParam( href, 'lang', val) + hash;
			}
			return false;
		},

		
		components: function() {
			$(window).trigger('resize');
			AppView.prototype.components();

			if ( is_touch_device() )
			{
				$('html').addClass('touch');
			}





			// Basic instantiation:
      		// $('input[type="text"].colorpicker').each(function(){
      			$('#header_background').colorpicker({format: 'hex8'})
      			$('#text_color').colorpicker({format: 'hex8'})
      			$('#primary_color').colorpicker({format: 'hex8'})
      			$('#secondary_color').colorpicker({format: 'hex8'})
      			// {
			      //   popover: false,
			      //   inline: true,
			      //   container: '#demo'
			      // });
      		// })


      		function goodbye(e) {
			    if(!e) e = window.event;
			    //e.cancelBubble is supported by IE - this will kill the bubbling process.
			    // e.cancelBubble = true;
			    // e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog

		    	if ( participant.id && ! isModerator )
		    	{
				    const payload = {
				    	participant_id: participant.id
				    };
				    axios.post(`/user-left/${participant.id}`, payload)
	                    .then(response => {
	                        // location.reload();
	                        console.log('exit')
	                    });
                }

			    //e.stopPropagation works in Firefox.
			    // if (e.stopPropagation) {
			    //     e.stopPropagation();
			    //     e.preventDefault();
			    // }
			}
			if ( $('#content').hasClass('content-live') ) {
				window.onbeforeunload=goodbye; 


				window.surveyInt = setInterval(function(){
					$.ajax({
						url: '/survey-check',
						type: 'get',
						dataType: 'json',
						success: function(result) {
							if ( result.enabled ) {
								$('.popup-survey').addClass('open')
								clearInterval(window.surveyInt)
							}
						}
					})	
				}, 5000)
			}
      		

		},

		

		render: function( request ) {
			var _this = this,
				$tmp = $('<div/>'),
				url = Backbone.history.fragment;
			// $('#footer').removeClass('active');


			if ( App.routesHit > 1 )
			{
				// $('#content').css({opacity:0});
				$tmp.load( App.baseURL + url + " #content", function( html ) {
	            	AppView.prototype.updateTitle(html);

	            	setTimeout(function(){
		                $('#content').html($tmp.find('#content').html());
		                $('#content').attr('class', $tmp.find('#content').attr('class'));
		                $tmp.html('');
		                
		                _this.$el.triggerHandler('content:loaded');
		                _this.components();
		                _this.renderPage(request);

		                $(window).trigger('resize');
		                // scroll top

						$('#main').scrollTo( 0, {duration: 200});	
						$('#content').scrollTo( 0, {duration: 200});	

						setTimeout( function(){
							$('body').trigger('page:loaded');
						}, 500);
						
					}, 300);
					
	            });		
			} else {
				$('body').trigger('page:loaded');
				_this.$el.triggerHandler('content:loaded');
                _this.renderPage(request);
				_this.components();
			}
		}, 

		
		renderPage: function(request) {
			var header_timeout, footer_timeout, didScrollInterval
			var _this = this;			

			if (0 && _this.$el.hasClass('content-backstage') ) 
			{

				$('.content-backstage .nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				  	// e.target // newly activated tab
				  	// e.relatedTarget // previous active tab
				  	window.location.hash = $(e.target).attr('href')
				})

				let hash = window.location.hash 
				if ( hash ) {
					// alert(`.content-backstage .nav-tabs a[href="#{$hash}"]`)
					$(`.content-backstage .nav-tabs a[href="${hash}"]`).click()
				}

				$('select#login-type').trigger('change');
			}
			
		},


		_onWindowResize: function(e){
			var 
				_this = this,
				w = $(window).width(),
				h = $(window).height();
			!App.debug || console.log('resize');
				
			var innerH = $('body, html').height(window.innerHeight),
	            w = $(window).width(),
	            h = $(window).outerHeight(true);

			// alert(h); alert(w);

			$('body, html').height(h);

			

		},

		


	});

	module.exports = DefaultView;
	// return DefaultView;

// });
