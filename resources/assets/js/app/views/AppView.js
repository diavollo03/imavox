const _ = require('underscore');
require('../utils');
// const Backbone = require('underscore');
// const $ = require('jquery');
// define([
// 	'jquery',
// 	'underscore',
//     'backbone',
//     'bootstrap',
//     'scrollto',
//     // 'utils',
//     // 'bootstrapselect',
//     'detectizr',
//     // 'wpcr3',
//     // 'jcookie'
//     // 'TweenMax'
// 	], function( $, _, Backbone ){
	// console.log('AppView loaded');
	// console.log(window.$)
		const AppView = Backbone.View.extend({
			el: $('body'),

			_clockTimer: '00:00',

			initialize: function(){
				var _this = this; 
				$(window).on('resize', _.bind(this._onWindowResize, this));				

				// $('#main').on('scroll', _.bind(this._onMainScroll, this));
				// $(document).on('scroll', _.bind(this._onWindowScroll, this));
				_this.$el.on('page:loaded', _.bind(this._onPageLoaded, this));

				_this.searchTimer = null;


				

			},

			_onPageLoaded: function(e){
				$('body').addClass('loaded');
				// $(window).trigger('resize');
			},

			events: {
				'submit .comment-form' : '_commentFormSubmitHandler',

				'touchend .item'		: '_touchItemTapHandler',

				'click .item .overlay a'			: '_statusLinkClickHandler',

				'submit .timer-form'		: '_timerFormSubmitHandler',

				'click #time-action-submit'	: '_timerActionSubmitHandler',

				'click .close-popup'		: '_closePopupClickHandler',

				'keyup #comment'			: '_commentTypeHandler',
				'blur #comment'			: '_commentTypeHandler',
				'focus #comment'			: '_commentTypeHandler',

				// 'scroll .chat-messages-warp' : 'chatScrollHandler',
	        },

	        chatScrollHandler: function(e) {
	        	var $chatWrap = $(e.currentTarget)
	        	console.log('x')
	        	$chatWrap.attr('data-autoscroll', '0')
	        },	

	        _commentTypeHandler: function(e)
	        {
	        	e.preventDefault();
	        	var $i = $(e.currentTarget),
	        		_this = this,
	        		max = isNaN(parseInt($i.attr('maxlength'))) ? 150 : $i.attr('maxlength'),
	        		l = $i.val().length;
        		if ( $('.text-counter').length )
        		{
	        		$('.text-counter').text(l + '/' + max);
	        	}
	        	return false;
	        },

	        _closePopupClickHandler: function(e) {
	        	e.preventDefault();
	        	var _this = this;
	        	_this.$el.removeClass('sent-success');
	        	return false;
	        },

	        _timerActionSubmitHandler: function(e) {
	        	e.preventDefault();
	        	console.log(window._clockTimer)
	        	var $ti = $('#time-input'),
	        		timerStr = window._clockTimer,
	        		tiActive = parseInt($ti.attr('data-timer-active')),
	        		_token = $('input[name=_token]').val(),
	        		_this = this;
        		// console.log($ti);
        		var timerArr = timerStr.split(':'),
        			m = timerArr[0],
        			s = timerArr[1],
        			timer = parseInt(m*60) + parseInt(s);

    			if ( timer > 0 )
    			{
    				// console.log(tiActive);
    				// console.log(parseInt(tiActive) ? '0' : '1');
    				var control = !isNaN(tiActive) && tiActive ? '0' : '1';
    				$.ajax({
    					url: '/ajax-timer-control',
    					type: 'post',
    					dataType: 'json',
    					data: {
    						control: control,
    						_token: _token
    					},
    					error: function(error) {
    						console.log(error)
    					},
    					success: function(jsonResult) {
    						if ( jsonResult.success )
    						{
    							if ( control == '1' )
    							{
									_this.timer(timer);
								} else {
									clearInterval(window.timerInterval);
								}
								$('#time-input').val( window._clockTimer );
								$ti.attr('data-timer-active', control);
    						} 
    					}
    				});
    			}


	        	return false;
	        },

	        _timerFormSubmitHandler: function(e) {
	        	e.preventDefault();

	        	var _this = this,
	        		$f = $(e.currentTarget),
	        		timerStr = $('#time-input').val(),
	        		_token = $('input[name=_token]').val();

        		var timerArr = timerStr.split(':'),
        			m = timerArr[0],
        			s = timerArr[1],
        			timer = parseInt(m*60) + parseInt(s);

        		window._clockTimer = timerStr;	

    			$.ajax({
    				url: '/ajax-timer',
    				type: 'post',
    				dataType: 'json',
    				data: {
    					_token: _token,
    					timer: timer
    				},
    				error: function(error) {
    					console.log(error)
    				},
    				success: function(jsonResult) {
    					if ( jsonResult.success )
    					{
    						clearInterval(window.timerInterval);
    					} else {
    						// console.log(jsonResult.errors);
    					}
    				}
    			});



	        	return false;
	        },

	        _touchItemTapHandler: function(e) {
	        	
	        	var $item = $(e.currentTarget);

	        	
	        	if ( $item.hasClass('hover') )
	        	{
	        		 
	        	} else {
	        		e.preventDefault();
	        		
	        		$item.addClass('hover');		
	        		return false;
	        	}

	        	


	        	// return false;
	        },

	        

	        _statusLinkClickHandler: function(e) {
	        	e.preventDefault();

	        	var $a = $(e.currentTarget),
	        		href = $a.attr('href'),
	        		toarchive = $a.hasClass('to-archive') ? 1 : 0,
	        		tolive = $a.hasClass('to-live') ? 1 : 0,
	        		_token = $('input[name=_token]').val(),
	        		_this = this;

        		if ( tolive && $('.live-blocks .item').length >= 6 )
        		{
        			$('.live-blocks h2').addClass('error');
    				return false;
        		}

        		$.ajax({
        			url: href,
        			data: {
        				tolive: tolive,
        				toarchive: toarchive,
        				_token: _token,
        			},
        			type: 'post',
        			dataType: 'json',
        			error: function( error) {
        				console.log('error', error);
        			},
        			success: function(json){
        				if (json.success)
        				{
        					if (! $('.live-speaker-blocks').length )
        					{
        						_this.refreshItems(true);
        					} else {
        						_this.refreshSpeakerItems(true);	
        					}
        				} else {
        					$('.live-blocks h2').addClass('error');
        				}
        			}
        		})

	        	return false;
	        },

	        refreshItems: function( force )
	        {
	        	var _this = this;
	        	$.ajax({
	        		url: '/ajax-moderation',
	        		type: 'get',
	        		dataType: 'json',
	        		success: function(jsonResult) {
	        			// $('.row-moderation').html($(jsonResult.html).find('.row-moderation').html());
	        			if ( force || $('.new-blocks .hover').length == 0 )
	        			{
	        				$('.new-blocks').html($(jsonResult.html).find('.new-blocks').html());
	        			}
	        			if ( force || $('.live-blocks .hover').length == 0 )
	        			{
	        				$('.live-blocks').html($(jsonResult.html).find('.live-blocks').html());
	        			}
	        			if ( force || $('.archived-blocks .hover').length == 0 )
	        			{
	        				$('.archived-blocks').html($(jsonResult.html).find('.archived-blocks').html());
	        			}


	        			_this._touchHandlers();
	        			window._clockTimer = jsonResult.clockTimer;
	        			if ( jsonResult.speakerSession.timer_active != '1' && window.timerInterval)
	        			{
	        				clearInterval(window.timerInterval);
	        				window.timerInterval = null;
	        				if ( $('#time-input').length )
	        				{
	        					$('#time-input').val( window._clockTimer );
	        				}
	        			}


	        		}
	        	})
	        },

	        refreshSpeakerItems: function( force )
	        {
	        	console.log('x')
	        	var _this = this;
	        	$.ajax({
	        		url: '/ajax-speaker',
	        		type: 'get',
	        		dataType: 'json',
	        		success: function(jsonResult) {
	        			
	        			if ( force || $('.live-speaker-blocks .hover').length == 0 )
	        			{
	        				$('.live-speaker-blocks').html($(jsonResult.html).find('.live-speaker-blocks').html());
	        			}
	        			_this._touchHandlers();

	        			if ( jsonResult.speakerSession.timer_active != '1' && window.timerInterval)
	        			{
	        				clearInterval(window.timerInterval);
	        				window.timerInterval = null;
	        				
	        			} else if ( jsonResult.speakerSession.timer_active != '0' && !window.timerInterval )
	        			{
	        				// console.log(jsonResult.speakerSession);
							var timerArr = jsonResult.clockTimer.split(':'),
	        					m = timerArr[0],
	        					s = timerArr[1],
	        					timer = parseInt(m*60) + parseInt(s);
        					_this.timer(timer)
	        			}
	        			if ( jsonResult.speakerSession.timer_active != '1' && $('.time-left').length )
						{
							$('.time-left').html( jsonResult.clockTimer );
						}
	        		}
	        	})
	        },

	        _commentFormSubmitHandler: function(e) {
	        	e.preventDefault();


	        	var $f = $(e.currentTarget),
	        		// name = $('#name-surname', $f).length ? $('#name-surname', $f).val().trim() : '',
        			comment = $('#comment').val().trim(),
        			_token = $('input[name=_token]').val().trim(),
        			moderator = $f.hasClass('moderator-form') ? 1 : 0,
        			errors = 0,
        			_this = this;


				if ( moderator )
				{
					name = 'Moderator';
				}
    			// if ( name.length == 0 || escapeHtml(name) != name )
    			// {
    			// 	$('#name-surname').parent().addClass('error');
    			// 	errors += 1;
    			// }
    			// console.log(escapeHtml(comment), escapeHtml(comment) != comment, comment.length == 0 || escapeHtml(comment) != comment);
    			if ( comment.length == 0 || escapeHtml(comment) != comment )
    			{
    				$('#comment').parent().addClass('error');
    				errors += 1;
    			}
    			// alert(1);
    			// console.log(errors);

    			if ( 0 == errors )
    			{

    				$.ajax({
    					url: '/ajax-record-comment',
    					type: 'post',
    					dataType: 'json', 
    					data: {
    						name: name,
    						content: comment,
    						moderator: moderator,
    						_token: _token
    					},
    					error: function(error) {
    						console.log(error)
    					},
    					success: function( json )
    					{
    						if ( json.success )
    						{
    							$('#name-surname').val('');
    							$('#comment').val('');
    							$('#main').addClass('comment-submitted-ok');
    							_this.$el.addClass('sent-success');
    							if ( moderator = $f.hasClass('moderator-form') )
    							{
    								_this.refreshItems( true );
    							}
    							setTimeout(function(){
    								$('#main').removeClass('comment-submitted-ok')	
    							}, 5000);
    						} else {
    							// console.log(json.errors);

    							setTimeout(function(){
    								$('#main').removeClass('comment-submitted-not-ok')	
    							}, 5000);
    						}
    					}
    				})
    			}





	        	return false;
	        },
			

			_linkClickHandler: function(e) {
		      	var $a, href;
		      	// return false;
		      	$a = $(e.currentTarget);
		      	href = $a.attr('href');

		      	if ( $a.attr('target') == 'blank' || $a.attr('target') == '_blank')
		      		return;
		      	//alert(Backbone.history.navigate(href.replace(Site.baseURL, ''), true));
				// this.loading(true);
				// setTimeout(function(){
				// 	Backbone.history.navigate(href.replace(Site.baseURL, ''), true);
				// }, 500);
		      	true === Backbone.history.navigate(href.replace(App.baseURL, ''), true) && this.loading(true);
		      	this.prepareHideMenu();
		      	// this.hideSubNav();
		      	e.preventDefault();
		      	return false;
		    },

		    

			loading: function( status ){
				var _this = this;
				if ( status )
				{
					// siteLoading = true;
					setTimeout(function(){
						// _this.$el.removeClass('loading page-is-changing');
					}, 1400);
					$('body').removeClass('loaded').addClass('loading');
					
					// _this.$el.removeClass('loaded').addClass('loading page-is-changing');
				} else {
					// $('body').removeClass('loading');
					// _this.$el.removeClass('loading').removeClass('page-is-changing');
					// siteLoading = false;
				}
			},

			updateTitle: function( html ) {
				
			},        

			
	        components: function( dontScroll ) {

	        	var _this = this;

	        	$('a.blank').attr('target', '_blank');
				$('body').removeClass('loading');

				if ( $('#content').hasClass('content-moderation') )
				{
					// console.log('x');	
					_this.ajaxInterval = setInterval(function(){
								_this.refreshItems( false );
							}, 1500);
				}
				if ( $('#content').hasClass('content-speaker') )
				{
					// console.log('x2');	
					_this.ajaxInterval = setInterval(function(){
								_this.refreshSpeakerItems();
							}, 1500);
				}
				


	        }, // end components()


	        timer: function( countDownDate )
	        {
	        	// Set the date we're counting down to
				
	        	var _this = this;
	        	var d = new Date();
				// d.setHours(0,0,0,0);
				var now = d.getTime();
	        	countDownDate = now + (countDownDate * 1000);
	        	// console.log(now, countDownDate);

				// Update the count down every 1 second
				window.timerInterval = setInterval(function() {

  					// Get todays date and time
  					// var d = new Date();
  					// d.setHours(0,0,0,0);
  					// var now = d.getTime();
  					var now = new Date().getTime();
					
  					// countDownDate = new Date


  					// if ( now != currentTime + 1 )
  					// {
  					// 	console.log(now - currentTime, now, currentTime, 'x');
  					// }
  					// currentTime = now;

  					// console.log(new Date());
  					// 1548782063426 
  					// 1550332395794 
  					// console.log( (countDownDate - now) / 1000 );
  					// console.log(now);

  					// Find the distance between now and the count down date
  					var distance = countDownDate - now;

  					// Time calculations for days, hours, minutes and seconds
  					var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  					var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  					// Display the result in the element with id="demo"
  					// document.getElementById("demo").innerHTML = days + "d " + hours + "h "
  					// 				+ minutes + "m " + seconds + "s ";
  					var time_str = atLeast2Digit(hours * 60 + minutes) + ':' + atLeast2Digit(seconds);
  					if ( distance >= 300000 )
  					{
  						if ( $('.time-left').length )
						{
	  						$('.speaker-timer').attr('class', 'speaker-timer good');	
	  						$('.time-left').html( time_str);
	  					}
	  					if ( $('input[name="time-input"]').length )
						{
							$('input[name="time-input"]').val(time_str);
						}
  					}
					
					// console.log(distance);
					if ( distance < 300000 && distance > 1 )
					{
						
						if ( $('.time-left').length )
						{
							$('.speaker-timer').attr('class', 'speaker-timer poor');
							$('.time-left').html( time_str );
						}
						if ( $('input[name="time-input"]').length )
						{
							$('input[name="time-input"]').val(time_str);
						}
					}

  					
  					if ( distance <= 1) {
    					clearInterval(window.timerInterval);
    					if ( $('.time-left').length )
						{
	    					$('.speaker-timer').attr('class', 'speaker-timer expired');
							$('.time-left').html('00:00');
						}
						if ( $('input[name="time-input"]').length )
						{
							$('input[name="time-input"]').val('00:00');
						}
  					}
				}, 1000);
	        },


	        

			render: function() {
				var _this = this;
				// _this.components();
				// console.log(Detectizr);
				// Detectizr.detect();
				// alert(Detectizr.touch);
				// if ( Detectizr.device.type != 'desktop')
				// {

				$('.chat-messages-warp').attr('data-autoscroll', 0)
	        

				if ( is_touch_device() ) {
					$('html').addClass('touch');
				} else {
					$('html').addClass('desktop');
				}

				_this._touchHandlers();

				
				// _this.components();
				$(window).trigger('resize');

				
				if ( $('#content').hasClass('content-moderation'))
				{

				}

				if ( $('#content').hasClass('content-speaker'))
				{
					// var countDownDate = new Date("Feb 16, 2019 17:35:25").getTime();	
					var countDownDate = $('.speaker-timer-container').data('timer-date');

					// console.log(countDownDate);
					// _this.timer(new Date(countDownDate).getTime());
					if ( $('.speaker-timer-container').data('timer-active') )
					{
						var timerArr = countDownDate.split(':'),
	        			m = timerArr[0],
	        			s = timerArr[1],
	        			timer = parseInt(m*60) + parseInt(s);
						_this.timer(timer);
					}
				}


				if ( $('#content').hasClass('content-moderation'))
				{
					if ( $('#time-input').data('timer-active'))
					{
						var countDownDate = $('#time-input').data('timer-date');
						var timerArr = countDownDate.split(':'),
	        			m = timerArr[0],
	        			s = timerArr[1],
	        			timer = parseInt(m*60) + parseInt(s);
						_this.timer(timer);
					}
				}

				


			},

			_touchHandlers: function()
			{
				// if ( Detectizr.device.type != 'desktop')
				if ( is_touch_device() )
				{
					$('.item').not('.initialised').each(function(){
		        		var $i = $(this);
		        		var content_area = $i[0];
		        		$(this).addClass('initialised');
						document.body.addEventListener("touchend", function(e) {
						  	var target = e.target || e.srcElement;
						  	if (target !== content_area && !isChildOf(target, content_area)) {
								// alert("You clicked outside the content area!");
								if ( $i.hasClass('hover') )
								{
									$i.removeClass('hover');
								}
						  	}
						}, false);
					});
				}
			},

			_onWindowScroll: function(){
				var st = $(window).scrollTop();
				
			},

			_onWindowResize: function(){
				var w = $(window).width(),
					h = $(window).height(),
					_this = this;



			}

		});

		// return AppView;
		module.exports = AppView;

// }); 