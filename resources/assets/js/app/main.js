import $ from 'jquery';
window.$ = $;
import _ from 'underscore';
// window._ = _;
import Backbone from 'backbone';
// window.Backbone = Backbone;
import { Router } from './router';


// require('imports-loader?this=>window!detectizr');
// window.Detectizr = Detectizr;

const AppView = require('./views/appView');
window.AppView = AppView;


window.App =
{
    baseURL: '/',//DEC.baseurl + '/',
    isRetina: window.devicePixelRatio > 1,
    root: '/',
    debug: !0,
    routesHit: 0
}
// alert(3)
// require([
    // 'app'
    // 'wp_util'
// ], function( App ){
	// import App from './app';
    // App.app = App;
    // App.app.initialize();
// });


// define([
//   'jquery',
//   'underscore',
//   'backbone',
//   'router'
// ], function($, _, Backbone, Router){
//   var initialize = function() {
//     // Pass in our Router module and call it's initialize function
Router.initialize();
  // }
  // return { 
      // initialize: initialize
    // };
// });
