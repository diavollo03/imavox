{{-- <chat-conversations></chat-conversations> --}}
          <div class="card card-default">
              <div class="card-header" @if ($speaker_session->primary_color) style="background-color: {{ $speaker_session->primary_color }}" @endif><h4 data-conv-id="{{ $conversationId }}">Live chat</h4></div>
              <div class="card-body">
                  
                  <div class="chats">
                      <div class="row">
                          <div class="col-md-12">
                              @if($conversationId)
                              <div class="w-100 py-2 chat-messages-warp" data-autoscroll="1">
                                  <chat-messages :conversation="{{ $conversationId }}"></chat-messages>
                              </div>
                              <div class="w-100">
                                  <chat-form
                                        :conversation="{{ $conversationId }}"
                                        :user="{{ auth()->user() }}"
                                  ></chat-form>
                              </div>
                              @endif
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      {{-- </div> --}}
      {{-- <div class="mb-2 col-md-3"> --}}
          {{-- @if($conversationId)
              <conversation-participants :conversation="{{ $conversationId }}"></conversation-participants>
          @endif --}}
      {{-- </div> --}}
  
      {{ csrf_field() }}
  {{-- </div> --}}
