<!doctype html>
<html lang="en-GB" class="">
<head>
    <meta charset="UTF-8">
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width" />

    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>QA - Imavox</title>
    {{-- Styles --}}
    <link href="{{ asset('assets/css/main.css?' . time()) }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    @yield ('head')
</head>
<body>
    <main id="main" class="main ">

		@yield ('content')
        
    </main>


    
    <script>
        /* <![CDATA[ */
        var currentTime = {{ time() }};
        var currentDateTime = '{{ date("j F, Y H:i:s") }}';
        /* ]]> */
    </script>
    <script>
    /* <![CDATA[ */
    var Site = { PusherEnabled: false };
    /* ]]> */
    </script>
    <script>
        var conversations = {!! isset($conversations) ? json_encode($conversations) : '{}' !!};
        var participant = {!! isset($participant) ? json_encode($participant) : '{}' !!};
    </script>

    <script>
      @auth
        window.isModerator = {!! json_encode(Auth::user()->hasRole(['moderator', 'admin']), true) !!};
      @else
        window.isModerator = false;
      @endauth
    </script>
    <script src="{{ asset('js/main.js?' . time() ) }}"></script>
    <script src="{{ asset('js/app.js?' . time() ) }}"></script>
</body>
</html>
