<!doctype html>
<html lang="en-GB" class="">
<head>
    <meta charset="UTF-8">
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width" />

    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>QA - Imavox</title>
    {{-- Styles --}}
    <link href="{{ asset('assets/css/main.css?' . time()) }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    
</head>
<body>
    <main id="main" class="main ">


    <header class="backstage-main-header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="/{{ Auth::user() && Auth::user()->hasRole('moderator') ? 'moderation' : ( Auth::user() && Auth::user()->hasRole('speaker') ? 'speaker' : 'backstage' ) }}">Imavox</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                @auth
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    
                    <div class="navbar-nav mr-auto">
                        @if ( Auth::user()->hasRole('admin') ) 
                        <a class="nav-item nav-link" href="/backstage">Live panel</a>
                        <a class="nav-item nav-link" href="/backstage/languages">Languages</a>
                        <a class="nav-item nav-link" href="/backstage/users">Users</a>
                        <a class="nav-item nav-link" href="/backstage/logs">Logs</a>
                        <a class="nav-item nav-link" href="/moderation">Moderation</a>
                        <a class="nav-item nav-link" href="/backstage/survey">Survey</a>
                        @endif
                        @if ( Auth::user()->hasRole('moderator') )
                        <a class="nav-item nav-link" href="/moderation">Moderation</a>
                        @endif
                    </div>
                    
                    <ul class="navbar-nav">

                        <li class="nav-item">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="get" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                     
                    </ul>
                </div>
               @endauth
            </div>
        </nav>
    </header>

        

		@yield ('content')
        
    </main>


    
    <script>
        /* <![CDATA[ */
        var currentTime = {{ time() }};
        var currentDateTime = '{{ date("j F, Y H:i:s") }}';
        /* ]]> */
    </script>
    <script>
    /* <![CDATA[ */
    var Site = { PusherEnabled: false };
    /* ]]> */
    </script>
    
    <script>
        var conversations = {!! isset($conversations) ? json_encode($conversations) : '{}' !!};
        var participant = {!! isset($participant) ? json_encode($participant) : '{}' !!};
    </script><script>
      @auth
        window.isModerator = {!! json_encode(Auth::user()->hasRole(['moderator', 'admin']), true) !!};
      @else
        window.isModerator = false;
      @endauth
    </script>
    <script src="{{ asset('js/main.js?' . time() ) }}"></script>
    <script src="{{ asset('js/app.js?' . time() ) }}"></script>
</body>
</html>
