@extends ('layouts.backstage-layout')

@section ('content')

<article id="content" class="content content-backstage">
	<div class="container">
		<header class="backstage-heading">
		<h1 class="page-title">Users</h1>
		
		</header>

		<div class="users-controls mb-4">
			{{-- <a class="btn btn-info" href="/backstage/users/add">Create user</a> --}}
			<h3>Edit user <b>{{ $user->name }}</b></h3>
		</div>

		<form action="/backstage/users/{{ $user->id }}/edit" method="post" autocomplete="off">
			{{ csrf_field() }}
			<div class="form-row mb-2">
				<div class="col-12 col-md-6">
					<div class="form-group">
					    <label for="email">Email address</label>
					    <input type="email" class="form-control" id="email" name="user-email" aria-describedby="emailHelp" placeholder="Enter email" value="{{ $user->email }}"  autocomplete="off">
					    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
					</div>
					
					<div class="form-group">
					    <label for="name">Name</label>
					    <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp" placeholder="Enter Name" value="{{ $user->name }}" autocomplete="off">
					    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
					</div>

					<div class="form-group">
					    <label for="role">Role</label>
					    <select class="form-control" id="role" name="role">
					    	@foreach ($roles as $role)
					    	<option value="{{ $role->id }}" @if ( $role->id == $user->roles()->first()->id ) selected="selected" @endif>{{ $role->description }}</option>
					    	@endforeach
					    </select>
					</div>

					<div class="form-check mb-3">
					    <input type="checkbox" class="form-check-input" id="change-password" name="change-password" value="1">
					    <label class="form-check-label" for="change-password">Change password</label>
					</div>

					<div class="form-group">
					    <label for="password">Password</label>
					    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
					</div>
					<div class="form-group">
					    <label for="password_confirmation">Password Confirmation</label>
					    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Password">
					</div>
				</div>
			</div>

			<button type="submit" class="btn btn-primary">Update</button>
		</form>

		@if ($errors->any())
		    <div class="alert alert-danger mt-3">
	            @foreach ($errors->all() as $error)
	                <div>{{ $error }}</div>
	            @endforeach
		    </div>
	    @elseif ( session()->has('message') )
	    	<div class="alert alert-success mt-3">
	    		{{ session()->get('message') }}
	    	</div>
		@endif

	</div>
</article>

@endsection