@extends ('layouts.backstage-layout')

@section ('content')

<article id="content" class="content content-backstage">
	<div class="container">
		<header class="backstage-heading">
		<h1 class="page-title">Users</h1>
		
		</header>

		<div class="users-controls mb-3">
			<a class="btn btn-info" href="/backstage/users/add">Create user</a>
			<a class="btn btn-info" href="/backstage/users/import">Import users</a>
		</div>

		@if ($errors->any())
		    <div class="alert alert-danger mt-3">
	            @foreach ($errors->all() as $error)
	                <div>{{ $error }}</div>
	            @endforeach
		    </div>
	    @elseif ( session()->has('message') )
	    	<div class="alert alert-success mt-3 mb-3">
	    		{{ session()->get('message') }}
	    	</div>
		@endif
		
		<table class="table">
		  	<thead>
		    	<tr>
		      		<th scope="col">#</th>
		      		<th scope="col">Email</th>
		      		<th scope="col">Name</th>
		      		<th scope="col">Role</th>
		      		<th scope="col">Created at</th>
		      		<th>Action</th>
		    	</tr>
		  </thead>
		  <tbody>
		  		@foreach ( $users as $user )
		    	<tr>
		      		<th scope="row">{{ $user->id }}</th>
		      		<td>{{ $user->email }}</td>
	      			<td>{{ $user->name }}</td>
	      			<td>{{ $user->roles()->first() ? $user->roles()->first()->description : ''}}</td>
		      		<td>{{ $user->created_at }}</td>
		      		<td><a href="/backstage/users/{{ $user->id }}/edit"><span class="oi oi-pencil"></span></a> 
		      			<a onclick="return window.confirm('Are you sure that you want to remove this user?')" href="/backstage/users/{{ $user->id }}/delete"><span class="oi oi-delete"></span></a></td>
		    	</tr>
	    		@endforeach
		  	</tbody>
		</table>

		<div class="pagination border-top pt-2">
			<hr>
			{{ $users->links() }}
		</div>

		
	</div>
</article>

@endsection