@extends ('layouts.backstage-layout')

@section ('content')

<article id="content" class="content content-backstage">
	<div class="container">
		<header class="backstage-heading">
		<h1 class="page-title">Users</h1>
		
		</header>

		<div class="users-controls mb-4">
			{{-- <a class="btn btn-info" href="/backstage/users/add">Create user</a> --}}
			<h3>Import users</h3>
		</div>

		<form action="/backstage/users/import" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-row mb-2">
				<div class="col-12 col-md-6">
					
					<div class="form-group">
					    <label for="import-file">CSV File</label>
					    <input type="file" class="form-control-file" id="import-file" name="import-file" aria-describedby="importHelp" placeholder="CSV File" >
					    <small id="importHelp" class="form-text text-muted">CSV file containing users (name, email, password)</small>
					</div>

				</div>
			</div>

			<button type="submit" class="btn btn-primary">Import</button>
		</form>

		@if ($errors->any())
		    <div class="alert alert-danger mt-3">
	            @foreach ($errors->all() as $error)
	                <div>{{ $error }}</div>
	            @endforeach
		    </div>
	    @elseif ( session()->has('message') )
	    	<div class="alert alert-success mt-3">
	    		{{ session()->get('message') }}
	    	</div>
		@endif

	</div>
</article>

@endsection