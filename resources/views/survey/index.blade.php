@extends ('layouts.backstage-layout')

@section ('content')

<article id="content" class="content content-backstage">
	<div class="container">
		<header class="backstage-heading">
		<h1 class="page-title">Survey form</h1>
		
		</header>

		<div class="users-controls mb-3">
			<a class="btn btn-info" href="/backstage/survey/export">Export</a>
			@if ( count($survey_entries) )
			<a class="btn btn-danger" href="/backstage/survey/clearall" onclick="return confirm('Are you sure you want to clear all the entries?')">Clear logs</a>
			@endif 
		</div>

		@if ( session()->has('message') )
	    	<div class="alert alert-success mt-3 mb-3">
	    		{{ session()->get('message') }}
	    	</div>
		@endif

		<table class="table">
		  	<thead>
		    	<tr>
		      		<th scope="col" style="width: 5%">#</th>
		      		<th scope="col" style="width: 5%">User</th>
		      		<th scope="col" style="width: 15%">#1 How interesting<br>did you find<br>the webinar?</th>
		      		<th scope="col" style="width: 15%">#2 Was the content<br>sufficient and relevant<br>to my work?</th>
		      		<th scope="col" style="width: 15%">#3 In what ways could this webinar be improved?</th>
		      		<th scope="col" style="width: 15%">#4 Would you like to attend more webinars from Brookfield &amp; JLL?</th>
		      		<th scope="col" style="width: 15%">#5 What other topics / webinars would you like to hear from Brookfield & JLL?</th>
		      		<th scope="col" style="width: 15%">Created at</th>
		      		<th>Action</th>
		    	</tr>
		  </thead>
		  <tbody>
		  		@foreach ( $survey_entries as $survey_entry )
		    	<tr>
		      		<th scope="row">{{ $survey_entry->id }}</th>
	      			<td>{{ $survey_entry->user ? $survey_entry->user->email : ''}}</td>
	      			<td>{{ $survey_entry->question_1 }}</td>
	      			<td>{{ $survey_entry->question_2 }}</td>
	      			<td>{{ $survey_entry->question_3 }}</td>
	      			<td>{{ $survey_entry->question_4 }}</td>
	      			<td>{{ $survey_entry->question_5 }}</td>
		      		<td>{{ $survey_entry->created_at }}</td>
		      		<td><a onclick="return window.confirm('Are you sure that you want to remove this entry?')" href="/backstage/survey/{{ $survey_entry->id }}/delete"><span class="oi oi-delete"></span></a></td>
		    	</tr>
	    		@endforeach
		  	</tbody>
		</table>

		<div class="pagination border-top pt-2">
			<hr>
			{{ $survey_entries->links() }}
		</div>

		
	</div>
</article>

@endsection