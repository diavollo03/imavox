@extends ('layouts.backstage-layout')

@section ('content')
	
<article id="content" class="content content-speaker">
	<div class="container">
		<div class="row">
			<div class="live-speaker-blocks blocks">
				@foreach ( $live_items as $item )
				<div class="block-column">
					<article class="block-item item @if ( $item->moderator ) block-item-moderator @endif ">
						<h3>{{ $item->name }}</h3>
						<div class="comment-text">
							<p>{{ $item->content }}</p>
						</div>
						{{-- <div class="date">{{ $item->created_at->format('j/m/Y - H:i') }}</div> --}}
						<div class="overlay">
							<a class="to-archive" href="/ajax-status/{{ $item->id }}"><span>Archive</span></a>
						</div>
					</article>
				</div>
				@endforeach
			</div><!-- /blocks -->
			<div class="speaker-timer-container" data-timer-date="{{ $clock_timer }}" data-timer-active="{{ $speaker_session->timer_active }}">
				<div class="speaker-timer @if ( $speaker_session->getRemainingInterval() > 300 )
												good 
											@elseif ($speaker_session->getRemainingInterval() > 1)  
												poor 
											@else 
												expired
											@endif">
					Time left: <span class="time-left">{{ $clock_timer }}</span>
				</div>
			</div>
		</div>
		<form>
			{{ csrf_field() }}
		</form>
	</div><!-- /container -->
	@if ( Auth::check() )
    <div class="logout-container" style="text-align: center"><a href="/logout">Log out</a></div>
    @endif
	
</article><!-- /content -->

@endsection
