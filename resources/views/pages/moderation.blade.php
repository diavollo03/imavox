@extends ('layouts.backstage-layout')

@section ('content')
	
<article id="content" class="content content-moderation">
	<div class="container">
		<header class="backstage-heading">
		<h1 class="page-title">Moderation panel
			{{-- @if ( Auth::check() ) --}}
	        {{-- <div class="logout-container"><a href="/logout">Log out</a></div> --}}
	        {{-- @endif --}}
		</h1>

		<div class="language-wrap">
			<select name="language" id="language">
				@foreach ( $languages as $language )
				<option value="{{ $language->code }}" {{ session('current_lang') == $language->code ? 'selected' : '' }}>{{ $language->title }}</option>
				@endforeach
			</select>
		</div>
		</header>
		
		
		<div class="row row-moderation">
			<div class="column-2">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
		  			<li class="nav-item">
		    			<a class="nav-link active" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="true">Moderate messages</a>
		  			</li>
		  			<li class="nav-item">
		    			<a class="nav-link" id="chat-tab" data-toggle="tab" href="#chatcontent" role="tab" aria-controls="chatcontent" aria-selected="false">Moderate chat</a>
		  			</li>

				</ul>
			</div>
			<div class="column">
  				<div class="live-embed-container">
					@if ( $lang_speaker_session->youtube_embed_code )
					<div class="embed-responsive embed-responsive-16by9">
							{!! $lang_speaker_session->youtube_embed_code !!}
					</div>
					@endif
				</div>
			</div>
		</div>
		
		<div class="tab-content backstage-tabs" id="backstage-tabs">
			<div class="tab-pane fade show active" id="messages" role="tabpanel" aria-labelledby="messages-tab">
				<div class="row row-moderation">
					<div class="column">
						<h2>New</h2>
						<div class="new-blocks blocks">
							@foreach ( $new_items as $item )
							<article class="block-item item">
								<h3>{{ $item->name }}</h3>
								<div class="comment-text">
									<p>{{ $item->content }}</p>
								</div>
								<div class="date">{{ $item->created_at->format('j/m/Y - H:i') }}</div>
								<div class="overlay">
									<a class="to-live" href="/ajax-status/{{ $item->id }}"><span>Go live</span></a>
									<a class="to-archive" href="/ajax-status/{{ $item->id }}"><span>Archive</span></a>
								</div>
							</article>
							@endforeach
							
						</div>
					</div>
					<div class="column">
						
						<div class="live-blocks blocks">
						<h2>Live ( {{ count($live_items) }}/6 )</h2>
							@foreach ( $live_items as $item )
							<article class="block-item item @if ( $item->moderator ) block-item-moderator @endif ">
								<h3>{{ $item->name }}</h3>
								<div class="comment-text">
									<p>{{ $item->content }}</p>
								</div>
								<div class="date">{{ $item->created_at->format('j/m/Y - H:i') }}</div>
								<div class="overlay">
									<a class="to-archive" href="/ajax-status/{{ $item->id }}"><span>Archive</span></a>
								</div>
							</article>
							@endforeach
						</div>
					</div>
					<div class="column">

						<h2>Archived</h2>
						<div class="archived-blocks blocks">
							@foreach ( $archived_items as $item )
							<article class="block-item item @if ( $item->moderator ) block-item-moderator @endif ">
								<h3>{{ $item->name }}</h3>
								<div class="comment-text">
									<p>{{ $item->content }}</p>
								</div>
								<div class="date">{{ $item->created_at->format('j/m/Y - H:i') }}</div>
								<div class="overlay">
									<a class="to-live" href="/ajax-status/{{ $item->id }}"><span>Go live</span></a>
								</div>
							</article>
							@endforeach
						</div>
					</div>	
				</div>
			</div>
			<div class="tab-pane fade" id="chatcontent" role="tabpanel" aria-labelledby="chat-tab">
				@auth
				@if ( empty($ajax ) )
				<div id="chatapp" class="chatapp pl-sm-2">
					@include('partials.chatapp')
				</div>
				@endif
				@endauth
			</div>
		</div>
	</div><!-- /container -->
	<div class="toolbar">
		<div class="container">
			<div class="toolbar-content">
				<div class="comment-form-container">
					<form action="" method="post" class="comment-form moderator-form" id="comment-form">
						{{ csrf_field() }}
						<div class="form-row">
							<div class="form-group form-group-comment">
								<input type="text" name="comment" id="comment" placeholder="Message for the speaker..." maxlength="150">
							</div>
							<div class="form-group form-group-submit">
								<button type="submit" name="comment-submit" id="comment-submit"><img src="/assets/img/sendWhite.png" alt="Send"></button>
							</div>
						</div>
					</form>
				</div>
				<div class="timer-container">
					<form class="timer-form" id="timer-form" method="post" action="">
						{{ csrf_field() }}
						<div class="form-row">
							<label>Timer</label>
							<input type="text" name="time-input" id="time-input" value="{{ $clock_timer }}" data-timer-active="{{ $speaker_session->timer_active }}" data-timer-date="{{ $clock_timer }}">
							<button type="button" id="time-action-submit"><img src="{{ asset('assets/img/playpause-white.png') }}" alt="Play/Pause"></button>
							<button type="submit" id="time-edit"><img src="{{ asset('assets/img/edit-white.png') }}" alt="Edit"></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</article><!-- /content -->

@endsection
