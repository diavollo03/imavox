@extends ('layouts.front-layout')

@section ('head')
<style type="text/css">
	@if ($speaker_session->primary_color)
	#chatapp .form-control.input-sm {
		 background-color: {{ $speaker_session->primary_color }}
	}	
	@endif
	@if ($speaker_session->secondary_color)
	#chatapp .btn-primary, 
	.live-container button[type=submit]
	{
		 background-color: {{ $speaker_session->secondary_color }}
	}	
	@endif

	@if ($speaker_session->text_color)
	#chatapp .form-control.input-sm,
	#btn-chat,
	#comment,
	#chatapp .card-default > .card-header,
	.live-container .form-group-counter,
	#chatapp .form-control.input-sm,
	.live-container button[type=submit],
	#chatapp .btn-primary#btn-chat,
	.logged-status, .logged-status a,
	#content .language-wrap select  {
		color: {{ $speaker_session->text_color }};
	}
	#content .language-wrap select  {
		border-color: {{ $speaker_session->text_color }};
	}

	#chatapp .form-control.input-sm {
		color: {{ $speaker_session->text_color }} !important;
	}
	::-webkit-input-placeholder {   
		color: {{ $speaker_session->text_color }} !important;
		opacity: 1;
	}
	:-ms-input-placeholder { 
		color: {{ $speaker_session->text_color }} !important;
		opacity: 1;
	}
	::placeholder { 
		color: {{ $speaker_session->text_color }} !important;
		opacity: 1;
	}
	@endif
	
</style>
@endsection

@section ('content')
	
<article id="content" class="content content-live">
	<header class="header" @if ($speaker_session->header_background) style="background-color: {{ $speaker_session->header_background }}" @endif >
		<div class="container-fluid">
			<div class="row no-gutters">
				<div class="col-6 col-sm-8">
					<h1 class="site-title">Header</h1>
					@if ($speaker_session->logo_path)
					<div class="logo-container"><img src="{{ asset('storage/' . str_replace('public', '', $speaker_session->logo_path)) }}" alt="Imavox"></div>
					@endif
				</div>
				<div class="col-6 col-sm-4">
					<div class="language-wrap">
						<select name="language" id="language">
							@foreach ( $languages as $language )
							<option value="{{ $language->code }}" {{ $current_language->code == $language->code ? 'selected' : '' }}>{{ $language->title }}</option>
							@endforeach
						</select>
					</div>
					<div class="logged-status pl-sm-2">
	                    <p>Logged in: {{ Auth::user()->name }} {{ Auth::user()->hasRole('moderator') ? '(moderator)' : '' }} </p>
	                    	                    
	                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
	                        {{ __('Logout') }}
	                    </a>

	                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                        @csrf
	                    </form>
	                </div>
	            </div>
			</div>
		</div>
	</header>
	<div class="container-fluid">
		<div class="row no-gutters">
			<div class="col- col">
				<div class="live-container">
					<div class="live-embed-container">
						@if ( $lang_speaker_session->youtube_embed_code )
						<div class="embed-responsive embed-responsive-16by9">
								{!! $lang_speaker_session->youtube_embed_code !!}
						</div>
						@endif
					</div>
					@if ($speaker_session->messages)
					<div class="comment-form-container" @if ($speaker_session->primary_color) style="background-color: {{ $speaker_session->primary_color }}" @endif>
						<form action="/record-comment" method="post" class="comment-form" id="comment-form">
							{{ csrf_field() }}
							<div class="form-row">
								{{-- <div class="form-group form-group-name">
									<input type="text" name="name-surname" id="name-surname" placeholder="Name &amp; Surname" value="">
								</div> --}}
								<div class="form-group form-group-comment">
									<input type="text" name="comment" id="comment" placeholder="Question or remark..." maxlength="150">
								</div>
								<div class="text-counter form-group-counter">
									0/150
								</div>
								<div class="form-group form-group-submit">
									<button type="submit" name="comment-submit" id="comment-submit">Send{{-- <img src="/assets/img/sendGrey.png" alt="Send"> --}}</button>
								</div>
							</div>
							<input type="hidden" name="moderator" value="{{ Auth::user()->hasRole('moderator') ? '1' : '0' }}">
						</form>
					</div>
					<div class="info-note">
						<p>Clicking on the <img width="22" src="{{ asset('assets/img/SmallSend.png') }}" alt="Send icon"> button will send your question/remark to the moderator</p>
					</div>
					@endif
				</div>
				
				

			</div>
			@if ($speaker_session->chat)
			<div class="col-md-4">

				@auth
				<div id="chatapp" class="chatapp pl-sm-2">
					@include('partials.chatapp')
				</div>
				@endauth
			</div>
			@endif
		</div>
		@if ($downloads)
		<section class="downloads">
			@foreach ($downloads as $download)
			<a href="/backstage/files/{{ $download->id }}/download"><i class="oi oi-data-transfer-download"></i> {{ $download->caption }}</a>
			@endforeach
		</section>
		@endif
		<footer class="footer">
			<div class="footer-banner">
				@if ($speaker_session->banner_path)
				<img src="{{ asset('storage/' . str_replace('public', '', $speaker_session->banner_path)) }}" alt="Imavox">
				@endif
			</div>
			@if ( session()->has('message') )
		    	<div class="alert alert-success mt-3 mb-3">
		    		{{ session()->get('message') }}
		    	</div>
			@endif
		</footer>

	</div><!-- /container -->
	<div class="popup-overlay" id="popup-overlay">
		<div id="popup-result" class="popup-success popup-result">
			<div class="popup-inner">
				<div class="success-message">Thank you for your message !</div>
			</div>
			<a class="close-popup" href=""><span class="icon-cancel"></span></a>
		</div>
	</div>

	<div class="popup-survey  {{ $errors->any() || session()->has('form-message') ? 'open' : '' }}" id="popup-survey">
		<div class="survey-form-container">
			<h5><b>JLL / Brookfield Real Estate Webinar 2020</b></h5>
			<h6 class="mb-4">25 June 2020, Thursday</h6>

			<form method="post" action="/survey">
				{{ csrf_field() }}
				@if ($errors->any())
				    <div class="alert alert-danger mt-3">
			            @foreach ($errors->all() as $error)
			                <div>{{ $error }}</div>
			            @endforeach
				    </div>
				    <a class="close-popup-form" href=""><span class="icon-cancel"></span></a>
			    @elseif ( session()->has('form-message') )
			    	<div class="alert alert-success mt-3">
			    		{{ session()->get('form-message') }}
			    	</div>
			    	<a class="close-popup-form" href=""><span class="icon-cancel"></span></a>
		    	@else
				<div class="form-group">
					<label for="question-1-group"><b>How interesting did you find the webinar?</b></label>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="question-1" id="question-1-1" value="Very" required>
						<label class="form-check-label" for="question-1-1">Very</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="question-1" id="question-1-2" value="Somewhat">
						<label class="form-check-label" for="question-1-2">Somewhat</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="question-1" id="question-1-3" value="Neutral">
						<label class="form-check-label" for="question-1-3">Neutral</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="question-1" id="question-1-4" value="Not very">
						<label class="form-check-label" for="question-1-4">Not very</label>
					</div>
				</div>
				<div class="form-group">
					<label for="question-2-group"><b>Was the content sufficient and relevant to my work?</b></label>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="question-2" id="question-2-1" value="Yes" required>
						<label class="form-check-label" for="question-2-1">Yes</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="question-2" id="question-2-2" value="Somewhat">
						<label class="form-check-label" for="question-2-2">Somewhat</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="question-2" id="question-2-3" value="No">
						<label class="form-check-label" for="question-2-3">No</label>
					</div>
				</div>
				<div class="form-group">
					<label for="question-3"><b>In what ways could this webinar be improved?</b></label>
					<textarea class="form-control" id="question-3" name="question-3" required></textarea>
				</div>
				<div class="form-group">
					<label for="question-4-group"><b>Would you like to attend more webinars from Brookfield &amp; JLL?</b></label>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="question-4" id="question-4-1" value="Yes" required>
						<label class="form-check-label" for="question-4-1">Yes</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="question-4" id="question-4-2" value="No">
						<label class="form-check-label" for="question-4-2">No</label>
					</div>
				</div>
				<div class="form-group">
					<label for="question-5"><b>What other topics / webinars would you like to hear from Brookfield & JLL?</b></label>
					<textarea class="form-control" id="question-5" name="question-5" required></textarea>
				</div>
				<div class="form-group">
					<button type="submit">Send</button>
				</div>

				@endif


				
			</form>
		</div>
	</div>
	
</article><!-- /content -->

@endsection
