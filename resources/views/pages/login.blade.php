{{-- @extends ('layouts.backstage-layout') --}}

@extends ('layouts.front-layout')

@section ('head')
<style type="text/css">
    @if ($speaker_session->header_background)
    .auth-form-container .card-body {
        background-color: {{ $speaker_session->header_background }}
    }
    @endif
    @if ($speaker_session->primary_color)
    #chatapp .form-control.input-sm {
         background-color: {{ $speaker_session->primary_color }}
    }   
    
    @endif
    @if ($speaker_session->secondary_color)
    #chatapp .btn-primary, 
    .live-container button[type=submit],
    .auth-form-container .btn-primary,
    .auth-form-container .card-header
    {
         background-color: {{ $speaker_session->secondary_color }}
    }   
    @endif

    @if ($speaker_session->text_color)
    #chatapp .form-control.input-sm,
    #btn-chat,
    #comment,
    #chatapp .card-default > .card-header,
    .live-container .form-group-counter,
    #chatapp .form-control.input-sm,
    .live-container button[type=submit],
    #chatapp .btn-primary#btn-chat,
    .logged-status, .logged-status a,
    .auth-form-container .card-header,
    .auth-form-container .btn-primary,
    .auth-form-container .card-body {
        color: {{ $speaker_session->text_color }};
    }

    #chatapp .form-control.input-sm {
        color: {{ $speaker_session->text_color }} !important;
    }
    ::-webkit-input-placeholder {   
        color: {{ $speaker_session->text_color }} !important;
        opacity: 1;
    }
    :-ms-input-placeholder { 
        color: {{ $speaker_session->text_color }} !important;
        opacity: 1;
    }
    ::placeholder { 
        color: {{ $speaker_session->text_color }} !important;
        opacity: 1;
    }
    @endif
    
</style>
@endsection

@section ('content')

<article id="content" class="content content-live">
    <header class="header" @if ($speaker_session->header_background) style="background-color: {{ $speaker_session->header_background }}" @endif >
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-6 col-sm-8">
                    <h1 class="site-title">Header</h1>
                    @if ($speaker_session->logo_path)
                    <div class="logo-container"><img src="{{ asset('storage/' . str_replace('public', '', $speaker_session->logo_path)) }}" alt="Imavox"></div>
                    @endif
                </div>
               
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row justify-content-center">
            <div class="auth-form-container">
            
                <div class="card">
                    <div class="card-header">{{ __('Login') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login.guest') }}">
                            @csrf

                            @if ( $speaker_session->login_type == 1 )
                            <div class="form-group row">
                                <label for="guest-name" class="col-md-4 col-form-label text-md-right">{{ __('Full name') }}</label>

                                <div class="col-md-6">
                                    <input id="guest-name" type="text" class="form-control{{ $errors->has('guest-name') ? ' is-invalid' : '' }}" name="guest-name" value="{{ old('guest-name') }}" required>

                                    @if ($errors->has('guest-name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('guest-name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Email address') }}</label>

                                <div class="col-md-6">
                                    <input id="guest-email" type="text" class="form-control{{ $errors->has('guest-email') ? ' is-invalid' : '' }}" name="guest-email" value="{{ old('guest-email') }}" required>

                                    @if ($errors->has('guest-email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('guest-email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            @elseif ( $speaker_session->login_type == 0 )
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Email address') }}</label>

                                    <div class="col-md-6">
                                        <input id="guest-email" type="text" class="form-control{{ $errors->has('guest-email') ? ' is-invalid' : '' }}" name="guest-email" value="{{ old('guest-email') }}" required>

                                        @if ($errors->has('guest-email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('guest-email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="language" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>

                                <div class="col-md-6">
                                    <select name="guest-language" id="guest-language" class="form-control-select custom-select">
                                        @foreach ( $languages as $language )
                                        <option value="{{ $language->id }}">{{ $language->title }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('guest-language'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('guest-language') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            {{-- <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div> --}}

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>

                                    {{-- @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
@endsection
