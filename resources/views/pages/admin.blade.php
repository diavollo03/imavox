@extends ('layouts.backstage-layout')

@section ('content')

<article id="content" class="content content-backstage">
	<div class="container">
		<header class="backstage-heading">
			<h1 class="page-title">Live panel</h1>
			<div class="language-wrap">
				<select name="language" id="language">
					@foreach ( $languages as $lang )
					<option value="{{ $lang->code }}" {{ session('current_lang') == $lang->code ? 'selected' : '' }}>{{ $lang->title }}</option>
					@endforeach
				</select>
			</div>
		</header>

	    <ul class="nav nav-tabs" id="myTab" role="tablist">
  			<li class="nav-item">
    			<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
  			</li>
  			<li class="nav-item">
    			<a class="nav-link" id="downloads-tab" data-toggle="tab" href="#downloads" role="tab" aria-controls="messages" aria-selected="false">Downloads</a>
  			</li>
  			<li class="nav-item">
    			<a class="nav-link" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">Messages</a>
  			</li>
		</ul>



		<div class="tab-content backstage-tabs" id="backstage-tabs">
  			<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
				<form action="/backstage/saveVideo" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-7 col-sm-9 col-xs-12">
							@if ( 'en_gb' == session('current_lang') )
							<div class="form-group">
								<label><span class="input-group-addon">Live video URL (Only for youtube)</span></label>
								<div class="input-group mb-3">
									<input type="text" class="form-control" aria-label="" name="video_url" value="{{ $speaker_session->youtube_url }}">
								</div>
							</div>
							<div class="form-group">
								<label><span class="input-group-addon">Live video embed code</span></label>
								<div class="input-group">
									<textarea class="form-control" aria-label="" name="video_embed_code" rows="8">{{ $speaker_session->youtube_embed_code }}</textarea>
								</div>
							</div>
							@else
							<div class="form-group">
								<label><span class="input-group-addon">Live video URL (Only for youtube) ( {{ $language->title }} )</span></label>
								<div class="input-group mb-3">
									<input type="text" class="form-control" aria-label="" name="video_url" value="{{ $lang_speaker_session->youtube_url }}">
								</div>
							</div>
							<div class="form-group">
								<label><span class="input-group-addon">Live video embed code ( {{ $language->title }} )</span></label>
								<div class="input-group">
									<textarea class="form-control" aria-label="" name="video_embed_code" rows="8">{{ $lang_speaker_session->youtube_embed_code }}</textarea>
								</div>
							</div>
							@endif
							<div class="form-group">
								<label for="header_background"><span class="input-group-addon">Header background</span></label>
								<div class="input-group mb-3">
									<input type="text" class="form-control" aria-label="" id="header_background" name="header_background" value="{{ $speaker_session->header_background }}">
								</div>
							</div>
							<div class="form-group">
								<label for="text_color"><span class="input-group-addon">Text color</span></label>
								<div class="input-group mb-3">
									<input type="text" class="form-control" aria-label="" id="text_color" name="text_color" value="{{ $speaker_session->text_color }}">
								</div>
							</div>
							<div class="form-group">
								<label for="primary_color"><span class="input-group-addon">Primary color</span></label>
								<div class="input-group mb-3">
									<input type="text" class="form-control" aria-label="" id="primary_color" name="primary_color" value="{{ $speaker_session->primary_color }}">
								</div>
							</div>
							<div class="form-group">
								<label for="secondary_color"><span class="input-group-addon">Secondary color</span></label>
								<div class="input-group mb-3">
									<input type="text" class="form-control" aria-label="" id="secondary_color" name="secondary_color" value="{{ $speaker_session->secondary_color }}">
								</div>
							</div>
							<div class="form-group">
								<label class="" for="logo-img">Logo Image</label>
								<input class="form-control-file" type="file" id="logo-img" name="logo-img">
								<div class="row">
									<div class="footer-banner-wrap col-sm-10 mt-3">
										@if (! empty($speaker_session->logo_path))
										<img style="width: 100%" src="{{ asset('storage/' . str_replace('public', '', $speaker_session->logo_path)) }}" alt="Logo">
										<div class="mt-1 mb-3"><a onclick="return confirm('Are your sure you want to remove this image?')" href="/backstage/saveVideo/removeBanner?banner=logo"><span class="oi oi-delete"></span> Remove logo</a></div>
										@endif 
									</div>
								</div>
						  	</div>
							<div class="form-group">
								<label class="" for="banner-img">Footer Banner Image</label>
								<input class="form-control-file" type="file" id="banner-img" name="banner-img">
								<div class="row">
								<div class="footer-banner-wrap col-sm-10 mt-3">
									@if (! empty($speaker_session->banner_path))
										{{-- <a class="btn btn-danger mb-1" href="/backstage/delete-image">Delete image</a> --}}
									<img style="width: 100%" src="{{ asset('storage/' . str_replace('public', '', $speaker_session->banner_path)) }}" alt="Footer Banner">
									<div class="mt-1 mb-3"><a onclick="return confirm('Are your sure you want to remove this image?')" href="/backstage/saveVideo/removeBanner?banner=footer"><span class="oi oi-delete"></span> Remove banner</a></div>
									@endif 
								</div>
								</div>
						  	</div>
							<div class="form-group">
								<div class="form-check">
							  		<input class="form-check-input" type="checkbox" id="enable-chat" name="enable-chat" value="1"  {{ $speaker_session->chat ? 'checked' : '' }}>
  									<label class="form-check-label" for="enable-chat">Enable Chat</label>
							  	</div>
						  	</div>
						  	<div class="form-group">
							  	<div class="form-check">
							  		<input class="form-check-input" type="checkbox" id="enable-messages" name="enable-messages" value="1" {{ $speaker_session->messages ? 'checked' : '' }}>
  									<label class="form-check-label" for="enable-messages">Enable Messages</label>
							  	</div>
							</div>
							<div class="form-group">
							  	<div class="form-check">
							  		<input class="form-check-input" type="checkbox" id="enable-survey" name="enable-survey" value="1" {{ $speaker_session->survey ? 'checked' : '' }}>
  									<label class="form-check-label" for="enable-survey">Enable Survey</label>
							  	</div>
							</div>

							<div class="form-group">
								<label for="login-type">Login type</label>
								<select class="form-control" name="login-type" id="login-type">
									<option value="0" {{ $speaker_session->login_type == 0 ? 'selected' : '' }}>Only registered emails</option>
									<option value="1"  {{ $speaker_session->login_type == 1 ? 'selected' : '' }}>Anyone can join</option>
								</select>
							</div>
							{{-- @if ( $speaker_session->login_type == 0 ) --}}
							<div class="form-group global-pass-container">
								<label for="global-password">Global password</label>
								<input type="text" name="global-password" id="global-password" class="form-control" placeholder="Password" value="{{ $speaker_session->password }}">
							</div>
							{{-- @endif  --}}

						</div>
						<div class="col-sm-3 col-md-5">
							@if ('en' == session('current_lang') )
								@if ( $speaker_session->youtube_embed_code )
								<div class="embed-responsive embed-responsive-16by9">
									{!! $output_embed !!}
								</div>
								@endif
							@else
							@if ( $lang_speaker_session->youtube_embed_code )
								<div class="embed-responsive embed-responsive-16by9">
								{!! $lang_output_embed !!}
								</div>
							@endif
							@endif
						</div>
						<div class="col-lg-12">
						 	<button type="submit" class="btn btn-primary">Submit</button>
						 </div>
					</div>
					
					@if ($errors->any())
					    <div class="alert alert-danger">
				            @foreach ($errors->all() as $error)
				                <div>{{ $error }}</div>
				            @endforeach
					    </div>
					@endif
				</form>
  			</div>
  			<div class="tab-pane fade" id="downloads" role="tabpanel" aria-labelledby="downloads-tab">
  				<div class="panel panel-default panel-downloads">
  					<h4 class="mb-3">Dowloads list</h4>
  					<table class="table">
			    		<thead>
			    			<th width="30%">Filename</th>
			    			<th width="40%">Caption</th>
			    			<th>Language</th>
			    			<th>Actions</th>
			    		</thead>
			    		<tbody>
  						@foreach ( $downloads as $download)
  							<tr>
  								<td>{{ $download->filename }}</td>
  								<td>{{ $download->caption }}</td>
  								<td>{{ $download->language->title }}</td>
  								<td>
  									<a href="/backstage/files/{{ $download->id }}/download"><i class="oi oi-data-transfer-download"></i></a> &nbsp;&nbsp;&nbsp;
  									<a href="/backstage/files/{{ $download->id }}/delete" onclick="return confirm('Are you sure that you want to remove this file?');"><i class="oi oi-delete"></i></a>
  								</td>
  							</tr>
			    		@endforeach
  						</tbody>
  					</table>

  					<h4 class="mb-3">Add file</h4>
  					<form enctype="multipart/form-data" action="/backstage/files/upload" method="post">
  						{{ csrf_field() }}
						<div class="row">
							<div class="col-md-7 col-sm-9 col-xs-12">
								<div class="form-group">
									<label class="" for="download-file">Upload File</label>
									<input class="form-control-file" type="file" id="download-file" name="download-file">
							  	</div>
							</div>
							<div class="col-md-7 col-sm-9 col-xs-12">
								<div class="form-group">
									<label class="" for="caption">Caption</label>
									<input class="form-control" type="text" id="caption" name="caption">
							  	</div>
							</div>
							<div class="col-lg-12">
							 	<button type="submit" class="btn btn-primary">Upload</button>
							 </div>
						</div>
						
						@if ($errors->any())
						    <div class="alert alert-danger">
					            @foreach ($errors->all() as $error)
					                <div>{{ $error }}</div>
					            @endforeach
						    </div>
						@endif
  					</form>
  				</div>
  			</div>
  			<div class="tab-pane fade" id="messages" role="tabpanel" aria-labelledby="messages-tab">
  				@if ( $comments )

				<div class="panel panel-default panel-comments">
				  		<!-- Default panel contents -->
				  		<div class="panel-heading mb-3">Posted Comments</div>
				  		<div class="panel-body mb-3">
				  			<div class="comments-buttons">
				    			<a href="/backstage/clear-comments" onclick="return confirm('Are you sure that you want to remove all comments?');"><button class="btn-danger">Clear all</button></a>
				    			<a href="/backstage/export-comments" ><button class="btn-success">Export CSV</button></a>
				    		</div>
				  		</div>

				  		<!-- Table -->
				  		<table class="table">
				    		<thead>
				    			<th>Name</th>
				    			<th>Text</th>
				    			<th>Status</th>
				    			<th>Actions</th>
				    		</thead>
				    		<tbody>
			    			@foreach ( $comments as $comment )
				    			<tr>
				    				<td>{{ $comment->name }}</td>
				    				<td>{{ $comment->content }}</td>
				    				<td>{{ $comment->status == 3 ? 'Archived' : ( $comment->status == 2 ? 'Live' : 'New' )  }}</td>
				    				<td><a href="/backstage/comment/{{ $comment->id }}/delete" title="Remove" onclick="return confirm('Are you sure that you want to remove this comment?');"><i class="oi oi-delete"></i></a></td>
				    			</tr>
			    			@endforeach
				    		</tbody>
				    	</table>

				  
				</div>

				@endif
  			</div>
		</div>


	</div>
</article>

@endsection