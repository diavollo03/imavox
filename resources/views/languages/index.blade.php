@extends ('layouts.backstage-layout')

@section ('content')

<article id="content" class="content content-backstage">
	<div class="container">
		<header class="backstage-heading">
		<h1 class="page-title">Languages</h1>
		
		</header>

		<div class="users-controls mb-3">
			<a class="btn btn-info" href="/backstage/languages/add">Create language</a>
		</div>

		@if ($errors->any())
		    <div class="alert alert-danger mt-3">
	            @foreach ($errors->all() as $error)
	                <div>{{ $error }}</div>
	            @endforeach
		    </div>
	    @elseif ( session()->has('message') )
	    	<div class="alert alert-success mt-3 mb-3">
	    		{{ session()->get('message') }}
	    	</div>
		@endif


		
		<table class="table">
		  	<thead>
		    	<tr>
		      		<th scope="col">#</th>
		      		<th scope="col">Language title</th>
		      		<th scope="col">Language code</th>
		      		<th scope="col">Default</th>
		      		<th scope="col">Created at</th>
		      		<th>Action</th>
		    	</tr>
		  </thead>
		  <tbody>
		  		@foreach ( $languages as $language )
		    	<tr>
		      		<th scope="row">{{ $language->id }}</th>
		      		<td>{{ $language->title }}</td>
	      			<td>{{ $language->code }}</td>
	      			<td>{{ $language->default ? 'Yes' : 'No' }}</td>
		      		<td>{{ $language->created_at }}</td>
		      		<td>@if ($language->id != 1)<a href="/backstage/languages/{{ $language->id }}/edit"><span class="oi oi-pencil"></span></a> 
		      			<a onclick="return window.confirm('Are you sure that you want to remove this language?')" href="/backstage/languages/{{ $language->id }}/delete"><span class="oi oi-delete"></span></a>@endif</td>
		    	</tr>
	    		@endforeach
		  	</tbody>
		</table>

		
	</div>
</article>

@endsection