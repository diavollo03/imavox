@extends ('layouts.backstage-layout')

@section ('content')

<article id="content" class="content content-backstage">
	<div class="container">
		<header class="backstage-heading">
		<h1 class="page-title">Languages</h1>
		
		</header>

		<div class="users-controls mb-4">
			{{-- <a class="btn btn-info" href="/backstage/users/add">Create user</a> --}}
			<h3>Edit language <b>{{ $language->title }}</b></h3>
		</div>

		<form action="/backstage/languages/{{ $language->id }}/edit" method="post" autocomplete="off">
			{{ csrf_field() }}
			<div class="form-row mb-2">
				<div class="col-12 col-md-6">
					<div class="form-group">
					    <label for="title">Title</label>
					    <input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp" placeholder="Enter title" value="{{ $language->title }}"  autocomplete="off">
					    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
					</div>
					
					<div class="form-group">
					    <label for="code">Code</label>
					    <input type="text" class="form-control" id="code" name="code" aria-describedby="codeHelp" placeholder="Enter language code" value="{{ $language->code }}" autocomplete="off">
					    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
					</div>


				</div>
			</div>

			<button type="submit" class="btn btn-primary">Update</button>
		</form>

		@if ($errors->any())
		    <div class="alert alert-danger mt-3">
	            @foreach ($errors->all() as $error)
	                <div>{{ $error }}</div>
	            @endforeach
		    </div>
	    @elseif ( session()->has('message') )
	    	<div class="alert alert-success mt-3">
	    		{{ session()->get('message') }}
	    	</div>
		@endif

	</div>
</article>

@endsection