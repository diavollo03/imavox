@extends ('layouts.backstage-layout')

@section ('content')

<article id="content" class="content content-backstage">
	<div class="container">
		<header class="backstage-heading">
		<h1 class="page-title">Languages</h1>
		
		</header>

		<div class="users-controls mb-4">
			{{-- <a class="btn btn-info" href="/backstage/users/add">Create user</a> --}}
			<h3>Add new language</h3>
		</div>

		<form action="/backstage/languages/add" method="post">
			{{ csrf_field() }}
			<div class="form-row mb-2">
				<div class="col-12 col-md-6">
					<div class="form-group">
					    <label for="title">Title</label>
					    <input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp" placeholder="Enter language title" value="">
					    {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
					</div>
					
					<div class="form-group">
					    <label for="code">Code</label>
					    <input type="text" class="form-control" id="code" name="code" aria-describedby="codeHelp" placeholder="Enter language code" value="">
					    <small id="codeHelp" class="form-text text-muted">Language code must be unique; the recommended format is en_gb.</small>
					</div>

				</div>
			</div>

			<button type="submit" class="btn btn-primary">Create</button>
		</form>

		@if ($errors->any())
		    <div class="alert alert-danger mt-3">
	            @foreach ($errors->all() as $error)
	                <div>{{ $error }}</div>
	            @endforeach
		    </div>
	    @elseif ( session()->has('message') )
	    	<div class="alert alert-success mt-3">
	    		{{ session()->get('message') }}
	    	</div>
		@endif

	</div>
</article>

@endsection