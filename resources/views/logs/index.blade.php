@extends ('layouts.backstage-layout')

@section ('content')

<article id="content" class="content content-backstage">
	<div class="container">
		<header class="backstage-heading">
			<h1 class="page-title">Logs</h1>
		</header>

		<div class="users-controls mb-3">
			<a class="btn btn-info" href="/backstage/logs/export">Export</a>
			@if ( count($logs) )
			<a class="btn btn-danger" href="/backstage/logs/clearall" onclick="return confirm('Are you sure you want to clear all the logs?')">Clear logs</a>
			@endif 
		</div>

		@if ( session()->has('message') )
	    	<div class="alert alert-success mt-3 mb-3">
	    		{{ session()->get('message') }}
	    	</div>
		@endif

		<table class="table">
		  	<thead>
		    	<tr>
		      		<th scope="col" style="width: 5%">#</th>
		      		<th scope="col" style="width: 10%">Type</th>
		      		<th scope="col" style="width: 15%">User</th>
		      		<th scope="col" style="width: 55%">Content</th>
		      		<th scope="col" style="width: 15%">Created at</th>
		      		{{-- <th>Action</th> --}}
		    	</tr>
		  </thead>
		  <tbody>
		  		@foreach ( $logs as $log )
		    	<tr>
		      		<th scope="row">{{ $log->id }}</th>
		      		<td>{{ $log->type }}</td>
	      			<td>{{ $log->user ? $log->user->email : ''}}</td>
	      			<td>{{ $log->body }}</td>
		      		<td>{{ $log->created_at }}</td>
		      		{{-- <td><a href="/backstage/logs/{{ $log->id }}/view"><span class="oi oi-eye"></span></a></td> --}}
		    	</tr>
	    		@endforeach
		  	</tbody>
		</table>

		<div class="pagination border-top pt-2">
			<hr>
			{{ $logs->links() }}
		</div>

	</div>
</article>

@endsection